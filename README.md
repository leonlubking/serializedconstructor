# Concept  
SerializedConstructor was created to allow polymorphic serialization that is configurable in the Unity inspector.  
SerializedConstructor is based on constructor serialization as opposed to instance serialization - you configure a constructor to use and the parameters that go along with it and can then instantiate an instance using that configuration.  
  
## Example  
Imagine you want to create equippable items that confer an effect while equipped:
    
	public class EquippableItem : MonoBehaviour
	{
		[SerializeField] private IEquipEffect equipEffect;		
	}
   
`equipEffect` will not show up in the editor because `IEquipEffect` is an interface, not a concrete implementation, and the editor does not support polymorphic serialization.  
SerializedConstructor does allow you to do this and will provide you with a dropdown in the editor where you can select a derived type, a constructor to use and what the parameters should be.  
Here's what the implementation looks like:
    
	public class EquippableItem 
	{		
		[SerializeField] private SerializableIEquipEffect serializableEquipEffect;

		public IEquipEffect EquipEffect => serializableEquipEffect.GetOrCreateInstance(); // You can also use CreateInstance() to use `serializableEquipEffect` as a factory
	}
	
	[Serializable]
	public class SerializableIEquipEffect : SerializedConstructor<IEquipEffect>
	{
		// Note: This class is only necessary because Unity refuses to directly serialize SerializedConstructor<IEquipEffect> because it contains a generic type.
	}
    
## Binding Example  
Sometimes you will want to use references or values in a constructor that are injected from outside of the configuration, for example when you want to pass a reference to another instance at run-time.  
You can use the `serializableEquipEffect.Bind(...)` to bind these references prior to calling `GetOrCreateInstance()` or `CreateInstance()`.  
You will need to add the `[Bind(...)]` attribute to the constructor parameter you want to use bound values for.  
  
  	/// <summary>
	/// An effect with a bound parameter.
	/// </summary>
	public class BoundEffect : IEquipEffect
	{
		public string Name { get; }
		public int Number { get; }

		public BoundEffect([Bind("name")] string name, int number)
		{
			this.Name = name;
			this.Number = number;
		}
	}

	public class EquippableItem : MonoBehaviour
	{
		[SerializeField] private SerializableIEquipEffect serializableEquipEffect;

		public IEquipEffect EquipEffect
		{
			get
			{
				serializableEquipEffect.Bind("name", name);
				return serializableEquipEffect.GetOrCreateInstance();
			}
		}
	}
    
The repo includes an example project with a scene called Example which implements the example above.  

# Integration  
In order to use SerializedConstructor in your own project simply copy the Plugins/SerializedConstructor folder.  
The excellent, open-source [Odin Serializer by Syrenix](https://github.com/TeamSirenix/odin-serializer/tree/master/OdinSerializer) is used to serialize everything. You will need to include it in your project to use SerializedConstructor.  
Once you have both you'll need to set the Scripting Runtime Version in Unity's Player Settings to `.NET 4.x equivalent` and the `Allow 'unsafe' Code` toggle on.  

# Custom Drawers  
There is some limited support for creating custom drawers for use in a SerializedConstructor hierarchy.  
Since you're inspecting constructor parameters and not an actual object instance it is only possible to create custom `PropertyDrawer`s for primitive types (`string`, `int`, etc.) and things like `Vector3`, `Vector3`, `Color`, etc - any class that requires its own constructor configuration cannot be custom drawn for.  
To do so make your custom `PropertyDrawer` implement the `IParameterDrawer` interface. In addition if it is an attribute drawer you will need to make that attribute valid for use on constructor parameters by adding the attribute to it from the following example:  

	[Serializable]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = true, AllowMultiple = false)]
	public class TypeDropdownAttribute : PropertyAttribute
	{
		...
	}

You can look at the `TypeDropdownAttribute` and `TypeDropdownAttributeDrawer` classes for an example of a custom `PropertyDrawer` for a `string` which lets you select a type in a dropdown.  

# Known Issues     
- SerializedConstructor should be considered in Beta - I have been using it while developing my own game but it has not been extensively tested.  
- Prefab variants (nested prefabs) not supported in Unity versions under 2019.2 due to a Unity bug -> https://issuetracker.unity3d.com/issues/serializable-field-in-prefab-variant-loses-all-data-after-being-edited  
- The inspector looks a little bit off, not exactly the same as the normal inspector. I just don't have the patience to fix this perfectly, if you can do better please feel free and make a merge request.  
- If you change any value in a SerializedConstructor property in a prefab instance then all of the values for the SerializedConstructor on that prefab instance will be marked overwritten. This is very difficult to fix and not planned any time soon or at all.  

# Attributes    
The following attributes can be used on constructor parameters.  
- `[Bind(Type type, string id, bool hideInInspector = true)]`  
Allows you to bind this at run-time. There is an example in the example section.  
- `[Default(Type selectedType, int selectedConstructorIndex, bool lockTypeSelection, bool lockConstructorSelection)]`  
Allows you to specifity a default type / constructor index and optionally disable selection. This greatly simplifies configuration when the desired value is already known.  
- `[ParameterTooltip(string text)]`  
Displays a tooltip when hovering over a label, just like a normal Tooltip attribute works for fields.  
- `[TypeDropdownAttribute(Type type, bool instantiableTypesOnly, bool allowMonoBehaviours, bool includeTargetType)]`  
Used on a string, a list of strings, a Type or a list of Type's, draws a dropdown where you can select a type that complies with the given constraints.  
- `[HideInInspector]`  
Hides the parameter in the inspector.  

# FAQ   
** What happens if the constructors change, type names change, etc.? **  
I`ve added validation that tries to pick the best suited constructor if the selected constructor can no longer be found or no longer matches.  
If the type changes then it is set to null.  

** Why didn't you just serialize whole instances? It's more straight forward than serializing constructors and you can more easily create custom inspectors. **  
This started off as an experiment - I theorized that this would be much faster since you only need Reflection to call a single constructor as opposed to Reflecting on each serialized field.  
Next to that I wanted to have an implementation that was minimally invasive to your code. Using constructor injection you don't need to change any of the code you want to serialize (unless you want custom drawers).  
Basically using this you can't even notice if the serialized class is being used in a SerializedConstructor or not.  
I also made the implementation so you don't need to make the root `MonoBehaviour` / `ScriptableObject` derive from [Odin's](https://github.com/TeamSirenix/odin-serializer/tree/master/OdinSerializer) `SerializedMonoBehaviour` / `SerializedScriptableObject` for the same reason - the implementation becomes much less invasive to the rest of your code base.  
In addition I wanted to be able to instantiate more than 1 instance with a single configuration so I could use them as a factory.  
If you do want to serialize whole instances and be able to create elaborate visualizations, take a look at the excellent [Odin - Inspector and Serializer by Syrenix](https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041). It's well worth the price!

** How fast is SerializedConstructor compared to X? **  
I haven't tested it for performance but it's built on the [Odin Serializer by Syrenix](https://github.com/TeamSirenix/odin-serializer/tree/master/OdinSerializer) (check their tests on their page) and I've added minimal overhead though I've yet to do an optimization pass.  
Do note; using a SerializedConstructor in a hierarchy based on a `ScriptableObject` is much faster than in a `GameObject` based hierarchy - the former uses binary serialization, the latter JSON serialization due to prefab modification issues.    
If you do any performance tests I'd be much interested in your results!  

** Cool stuff, could you add feature X to make it suit my needs? **  
SerializedConstructor is a side-project I've created due to needs in my own project, which is my main focus. I`ll fix reported bugs but will only drive development of SerializedConstructor if my own project requires it or if there is overwhelming interest.  
You are very welcome to make your own additions and provide merge requests.  

** Can I use / modify SerializedConstructor in my own project and release it? **  
Yes. Please check the terms of the Apache License, Version 2.0. It should allow you to do anything you like, provided you don't remove the license.  
A brief, clearly understandable version can be found [here](https://tldrlegal.com/license/apache-license-2.0-%28apache-2.0%29)

** The [Odin Serializer by Syrenix](https://github.com/TeamSirenix/odin-serializer/tree/master/OdinSerializer) allows serialization of type X but it doesn't show in the inspector, what gives? **  
I didn't implement all the supported types in the inspector because I haven't needed them all yet. Feel free to add support and create a merge request.  