﻿namespace Indigo.Examples
{
	/// <summary>
	/// An effect that confers an amount of a <see cref="CharacterAttribute"/> to the equipper.
	/// </summary>
	public class AttributeEffect : IEquipEffect
	{
		public CharacterAttribute Attribute { get; }
		public int Amount { get; }

		public AttributeEffect(CharacterAttribute attribute, int amount)
		{
			Attribute = attribute;
			Amount = amount;
		}
	}
}