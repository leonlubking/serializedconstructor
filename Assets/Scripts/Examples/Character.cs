﻿using System.Collections.Generic;
using UnityEngine;

namespace Indigo.Examples
{
	/// <summary>
	/// Our hero! A character in a game that can equip items.
	/// </summary>
	public class Character : MonoBehaviour
	{
		#pragma warning disable 0649
		[SerializeField] private List<EquippableItem> EquippedItems;
		#pragma warning restore 0649

		protected void Awake()
		{
			foreach(EquippableItem equippedItem in EquippedItems)
			{
				string log = "Character has equipped " + equippedItem.name;
				if(equippedItem.EquipEffect != null)
				{
					log += " which confers a(n) " + equippedItem.EquipEffect.GetType().Name;
				}
				else
				{
					log += " which confers no effects";
				}
				Debug.Log(log);
			}
		}
	}
}