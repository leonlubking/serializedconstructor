﻿namespace Indigo.Examples
{
	/// <summary>
	/// An effect that confers the ability to double jump to the equipper.
	/// </summary>
	public class DoubleJumpEffect : IEquipEffect
	{
		public float JumpHeight { get; }

		public DoubleJumpEffect(float jumpHeight)
		{
			JumpHeight = jumpHeight;
		}
	}
}