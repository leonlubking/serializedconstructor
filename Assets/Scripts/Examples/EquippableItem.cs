﻿using System;
using UnityEngine;

namespace Indigo.Examples
{
	/// <summary>
	/// An item that confers an effect on the user while equipped.
	/// </summary>
	[Serializable]
	public class EquippableItem : MonoBehaviour
	{
		// This will not show up in the editor because the editor does not support polymorphic serialization :(
		[SerializeField] private IEquipEffect equipEffect;

		// This will show up in the editor as a dropdown where you can select the implementation of the IEquippEffect
		// you want to use. You can then configure the constructor and its parameters in the editor.
		// Call serializableEquipEffects.CreateInstance() to create the instance;
		#pragma warning disable 0649
		[SerializeField] private SerializableIEquipEffect serializableEquipEffect;
		#pragma warning restore 0649

		public IEquipEffect EquipEffect
		{
			get
			{
				serializableEquipEffect.Bind("name", name);
				return serializableEquipEffect.GetOrCreateInstance();
			}
		}
	}
}