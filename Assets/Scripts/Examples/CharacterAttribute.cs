﻿namespace Indigo.Examples
{
	/// <summary>
	/// Attributes a character can posses.
	/// </summary>
	public enum CharacterAttribute
	{
		STRENGTH,
		DEXTERITY,
		CONSTITUTION,
		INTELLIGENCE,
		WISDOM,
		CHARISMA
	}
}