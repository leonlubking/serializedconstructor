﻿using Indigo.Serialization;
using System;

namespace Indigo.Examples
{
	/// <summary>
	/// An effect with a bound parameter.
	/// </summary>
	public class BoundEffect : IEquipEffect
	{
		public string Name { get; }
		public int Number { get; }

		public BoundEffect([Bind("name")] string name, int number)
		{
			this.Name = name;
			this.Number = number;
		}
	}
}