﻿using Indigo.Serialization;
using System;

namespace Indigo.Examples
{
	// This class is required only because Unity refuses to serialize SerializedConstructor<IEquipEffect> directly =/
	[Serializable]
	public class SerializableIEquipEffect : SerializedConstructor<IEquipEffect>
	{
	}
}