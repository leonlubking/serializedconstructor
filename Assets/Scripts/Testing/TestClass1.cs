﻿// <copyright file="TestClass1.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using UnityEngine;

namespace Test
{
	public class TestClass1 : ITestClass
	{
		public TestClass1(string stringParam, int intParam, double doubleParam, Color colorParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 0");
		}

		public TestClass1(string stringParam, int intParam, double doubleParam, float floatParam,
			Color colorParam, Vector2 vector2Param, Vector3 vector3Param, Sprite sprite)
		{
			Debug.Log("Constructed TestClass1 with constructor 1");
		}

		public TestClass1(string[] stringArrayParam, int[] intArrayParam, double[] doubleArrayParam, Color[] colorArrayParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 2");
		}

		public TestClass1(string[] stringArrayParam, int[] intArrayParam, ITestClass nestedTestClassParameter, double[] doubleArrayParam, Color[] colorArrayParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 3");
		}

		public TestClass1(int[] intArrayParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 4");
		}

		public TestClass1(List<int> intListParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 5");
		}

		public TestClass1(List<string> stringListParam, List<int> intListParam, List<double> doubleListParam, List<Color> colorListParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 6");
		}

		public TestClass1(TestClassMonoBehaviour monoBehaviourParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 7");
		}

		public TestClass1(TestClassScriptableObject testClassScriptableObject)
		{
			Debug.Log("Constructed TestClass1 with constructor 8");
		}

		public TestClass1(ITestClass nestedTestClassParameter)
		{
			Debug.Log("Constructed TestClass1 with constructor 9");
		}

		public TestClass1(int intParameter, Bounds boundsParameter, string stringParameter)
		{
			Debug.Log("Constructed TestClass1 with constructor 10");
		}

		public TestClass1(int intParameter, Bounds boundsParameter, string stringParameter, Rect rectParameter, float floatParameter)
		{
			Debug.Log("Constructed TestClass1 with constructor 11");
		}

		public TestClass1(int intParameter, Bounds boundsParameter, string stringParameter, Rect rectParameter, float floatParameter, Sprite spriteParameter, double doubleParam)
		{
			Debug.Log("Constructed TestClass1 with constructor 12");
		}

		public TestClass1(Sprite spriteParameter)
		{
			Debug.Log("Constructed TestClass1 with constructor 13");
		}

		public TestClass1(TestEnum enumParameter)
		{
			Debug.Log("Constructed TestClass1 with constructor 14");
		}
	}
}