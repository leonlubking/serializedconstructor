﻿// <copyright file="TestClass2.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using Indigo.Serialization;
using UnityEngine;

namespace Test
{
	public class TestClass2 : ITestClass
	{
		public TestClass2(bool boolParam, string stringParam, int intParam, double doubleParam, Color colorParam)
		{
			Debug.Log("Constructed TestClass2 with constructor 1");
		}

		public TestClass2(TestClass1 nestedTestClass1Parameter)
		{
			Debug.Log("Constructed TestClass2 with constructor 2");
		}

		public TestClass2([TypeDropdown(typeof(ITestClass))] string typeParam)
		{
			Debug.Log("Constructed TestClass2 with constructor 3");
		}

		public TestClass2(TestFlagsEnum flagsEnum)
		{
			Debug.Log("Constructed TestClass2 with constructor 4");
		}
		
		public TestClass2([HideInInspector] bool boolParam, [Bind("test")] string stringParam, [HideInInspector] int intParam, double doubleParam)
		{
			Debug.Log("Constructed TestClass2 with constructor 5 with bound parameter value " + stringParam);
		}
	}
}
 