// <copyright file="Test.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using UnityEngine;

namespace Test
{
	/// <summary>
	/// This script lets you configure a type and constructor for an ITestClass and then 
	/// constructs an instance using that configuration.
	/// </summary>
	public class Test : MonoBehaviour
	{
		public ITestClass TestClass
		{
			get
			{
				serializedInstance.Bind("test", stringToBind);
				return serializedInstance.GetOrCreateInstance();
			}
		}
		#pragma warning disable 0649
		[SerializeField] private SerializableITestClass serializedInstance;
		#pragma warning restore 0649

		[SerializeField] private string stringToBind = "test123";

		public ITestClass[] TestClassArray
		{
			get
			{
				if (testClassArray == null)
				{
					testClassArray = new ITestClass[serializedInstanceArray.Length];
					for (int i = 0; i < serializedInstanceArray.Length; i++)
					{
						serializedInstanceArray[i].Bind("test", stringToBind);
						testClassArray[i] = serializedInstanceArray[i].GetOrCreateInstance();
					}
				}

				return testClassArray;
			}
		}
		private ITestClass[] testClassArray;
		#pragma warning disable 0649
		[SerializeField] private SerializableITestClass[] serializedInstanceArray;
		#pragma warning restore 0649

		protected void Awake()
		{
			Debug.Log("created instance: " + TestClass);
			Debug.Log("created instance array: " + TestClassArray + " with " + TestClassArray.Length + " elements.");
		}
	}
}