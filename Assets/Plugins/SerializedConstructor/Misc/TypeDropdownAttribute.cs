// <copyright file="TypeDropdownAttribute.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using UnityEngine;

namespace Indigo.Misc
{
	/// <summary>
	/// Add this to a string field to to display a type dropdown of types derived from the given type which
	/// will be serialized by its fully qualified name.
	/// Will also work on Type fields when used for constructor parameters configured using a SerializedConstructor.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = true, AllowMultiple = false)]
	public class TypeDropdownAttribute : PropertyAttribute
	{
		public readonly Type Type;
		public readonly bool InstantiableTypesOnly;
		public readonly bool IncludeTargetType;
		public readonly bool AllowMonoBehaviours;

		public TypeDropdownAttribute(Type type, bool instantiableTypesOnly = true, bool allowMonoBehaviours = true, bool includeTargetType = false)
		{
			Type = type;
			InstantiableTypesOnly = instantiableTypesOnly;
			AllowMonoBehaviours = allowMonoBehaviours;
			IncludeTargetType = includeTargetType;
		}
	}
}