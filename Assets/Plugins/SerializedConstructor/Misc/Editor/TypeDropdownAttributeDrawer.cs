﻿// <copyright file="TypeDropdownAttributeDrawer.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Serialization;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Indigo.Misc
{
	[CustomPropertyDrawer(typeof(TypeDropdownAttribute))]
	public class TypeDropdownAttributeDrawer : PropertyDrawer, IParameterDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			TypeDropdownAttribute typeDropdownAttribute = attribute as TypeDropdownAttribute;

			Type typeFilter = typeDropdownAttribute.Type;
			bool instantiableTypesOnly = typeDropdownAttribute.InstantiableTypesOnly;
			bool allowMonoBehaviours = typeDropdownAttribute.AllowMonoBehaviours;
			bool includeTypeFilter = typeDropdownAttribute.IncludeTargetType;

			property.stringValue = DrawTypeSelection(position, null, typeFilter, instantiableTypesOnly,
				allowMonoBehaviours, property.stringValue, includeTypeFilter).AssemblyQualifiedName;
		}

		public static Type DrawTypeSelection(Rect position, GUIContent label, Type typeFilter,
			bool instantiableTypesOnly, bool allowMonoBehaviours, string currentSelection,
			bool includeTypeFilter = false, bool allowNone = false, bool drawBold = false)
		{
			if (typeFilter == null)
			{
				Debug.Log("typeFilter cannot be null.");
				return null;
			}

			int selectedIndex = 0;
			List<Type> possibleTypes = TypeHelper.GetAllTypesDerivedFrom(typeFilter, instantiableTypesOnly,
				allowMonoBehaviours, includeTypeFilter);
			possibleTypes.Sort((a, b) => a.Name.CompareTo(b.Name));
			List<GUIContent> possibleTypeNames = new List<GUIContent>(possibleTypes.Count);

			int startIndex = 0;
			if (allowNone)
			{
				possibleTypes.Insert(0, null);
				possibleTypeNames.Insert(0, new GUIContent("None"));
				startIndex++;
			}

			for (int i = startIndex; i < possibleTypes.Count; i++)
			{
				Type possibleType = possibleTypes[i];

				if (possibleType.AssemblyQualifiedName == currentSelection)
				{
					selectedIndex = possibleTypeNames.Count;
				}
				possibleTypeNames.Add(new GUIContent(possibleType.Name, ""));
			}

			if (possibleTypeNames.Count == 0)
			{
				Debug.LogError("No valid derived type found for " + typeFilter.GetGenericName());
				return null;
			}
			if (selectedIndex < 0 || selectedIndex >= possibleTypeNames.Count)
			{
				Debug.LogError("Invalid selected index " + selectedIndex + " found for " + typeFilter.GetGenericName());
				return null;
			}
			
			FontStyle labelFontStyle = EditorStyles.label.fontStyle;
			EditorStyles.label.fontStyle = drawBold ? FontStyle.Bold : labelFontStyle;
			Type returnValue = possibleTypes[EditorGUI.Popup(position, label, selectedIndex, possibleTypeNames.ToArray())];
			EditorStyles.label.fontStyle = labelFontStyle;
			return returnValue;
		}

		public float GetParameterHeight(Attribute attribute, float lineHeight, object currentValue, string parameterName)
		{
			return lineHeight;
		}

		public object DrawParameter(ParameterInfo parameterInfo, Attribute attribute, Rect position, object currentValue,
			string parameterName, string tooltip = null)
		{
			TypeDropdownAttribute typeDropdownAttribute = attribute as TypeDropdownAttribute;

			Type typeFilter = typeDropdownAttribute.Type;
			bool instantiableTypesOnly = typeDropdownAttribute.InstantiableTypesOnly;
			bool allowMonoBehaviours = typeDropdownAttribute.AllowMonoBehaviours;
			bool includeTypeFilter = typeDropdownAttribute.IncludeTargetType;

			return DrawTypeSelection(position, new GUIContent(parameterName, tooltip), typeFilter, instantiableTypesOnly,
				allowMonoBehaviours, (string)currentValue, includeTypeFilter).AssemblyQualifiedName;
		}
	}
}