// <copyright file="TypeExtensions.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Indigo.Misc
{
	public static class TypeExtensions
	{
		public static FieldInfo GetFieldViaPath(this Type type, string path)
		{
			int arrayIndex = path.IndexOf(".Array.data[");
			if(arrayIndex != -1)
			{
				path = path.Substring(0, arrayIndex);
			}

			Type parentType = type;
			string[] perDot = path.Split('.');
			FieldInfo fieldInfo = null;
			foreach (string fieldName in perDot)
			{
				fieldInfo = parentType.GetField(fieldName);
				if (fieldInfo == null)
				{
					fieldInfo = GetFirstNonPublicFieldInfoInhierarchy(parentType, fieldName);
				}

				if (fieldInfo != null)
				{
					parentType = fieldInfo.FieldType;
				}
				else
				{
					return null;
				}
			}

			return fieldInfo;
		}

		public static FieldInfo GetFirstNonPublicFieldInfoInhierarchy(this Type type, string fieldName)
		{
			FieldInfo fieldInfo = null;

			while(fieldInfo == null && type != null)
			{
				fieldInfo = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
				type = type.BaseType;
			}

			return fieldInfo;
		}

		public static bool IsList(this Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>);
		}

		/// <summary>
		/// Returns the full written out name for a type, excluding namespace but including generic type names.
		/// </summary>
		/// <param name="type">The target type.</param>
		/// <returns>The full written out name for a type, excluding namespace but including generic type names.</returns>
		public static string GetGenericName(this Type type)
		{
			if (type.IsArray)
			{
				return type.GetElementType().GetGenericName() + "[]";
			}
			else if (type.IsGenericType)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append(type.Name.Substring(0, type.Name.Length - 2));
				for (int index = 0; index < type.GetGenericArguments().Length; index++)
				{
					if(index == 0)
					{
						sb.Append("<");
					}
					Type argument = type.GetGenericArguments()[index];
					if (index < type.GetGenericArguments().Length - 1)
					{
						sb.Append(argument.GetGenericName() + ", ");
					}
					else
					{
						sb.Append(argument.GetGenericName() + ">");
					}
				}
				return sb.ToString();
			}

			return type.Name;
		}

		public static bool CheckIsStatic(this Type type)
		{
			return type.IsAbstract && type.IsSealed;
		}

		public static bool CheckImplementsGenericInterface(this Type type, Type genericInterfaceType)
		{
			foreach (Type interfaceType in type.GetInterfaces())
			{
				if (interfaceType.IsGenericType &&
					interfaceType.GetGenericTypeDefinition() == genericInterfaceType)
				{
					return true;
				}
			}

			return false;
		}

		public static bool CanBeSerializedAsValue(this Type type)
		{
			if (type == typeof(double) ||
				type == typeof(float) ||
				type == typeof(int) ||
				type == typeof(long) ||
				type == typeof(string) ||
				type == typeof(bool) ||
				type == typeof(Vector2) ||
				type == typeof(Vector3) ||
				type == typeof(Vector4) ||
				type == typeof(Rect) ||
				type == typeof(Bounds) ||
				type == typeof(Color) ||
				type.IsEnum)
			{
				return true;
			}
			else if (type.IsArray)
			{
				return type.GetElementType().CanBeSerializedAsValue();
			}
			else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
			{
				Type elementType = type.GetGenericArguments()[0];
				return elementType.CanBeSerializedAsValue();
			}

			return false;
		}

		public static bool CheckInstantiable(this Type type)
		{
			if (typeof(Component).IsAssignableFrom(type))
			{
				return false;
			}

			if (type == typeof(string) || type.IsValueType)
			{
				return true;
			}

			return !type.IsInterface &&
				!type.IsGenericTypeDefinition &&
				!type.IsAbstract &&
				type.IsVisible;
		}
	}
}