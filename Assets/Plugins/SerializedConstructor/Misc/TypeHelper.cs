// <copyright file="TypeHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Indigo.Misc
{
	/// <summary>
	/// Helper methods related to types.
	/// </summary>
	public static class TypeHelper
	{
		private static Dictionary<Assembly, Type[]> cachedAssemblies
		{
			get
			{
				if (_cachedAssemblies == null || _cachedAssemblies.Count == 0)
				{
					_cachedAssemblies = new Dictionary<Assembly, Type[]>();
					foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
					{
						if (a.FullName.StartsWith("UnityEditor") ||
							a.FullName.StartsWith("UnityTest") ||
							a.FullName.StartsWith("Assembly-CSharp-Editor") ||
							a.FullName.StartsWith("Castle.Core") ||
							a.FullName.StartsWith("nunit.framework"))
						{
							continue;
						}

						try
						{
							_cachedAssemblies.Add(a, a.GetTypes());
						}
						catch (ReflectionTypeLoadException ex)
						{
							_cachedAssemblies.Add(a, ex.Types);
						}
					}
				}

				return _cachedAssemblies;
			}
		}
		private static Dictionary<Assembly, Type[]> _cachedAssemblies = new Dictionary<Assembly, Type[]>();

		public static List<Type> GetAllTypesDerivedFrom(Type targetType, bool mustBeInstantiable = false,
			bool allowMonoBehaviours = true, bool includeTargetType = false)
		{
			if (targetType == null)
			{
				Debug.Log("targetType cannot be null.");
				return null;
			}

			List<Type> derivedTypes = new List<Type>();
			foreach (KeyValuePair<Assembly, Type[]> assemblyKVP in cachedAssemblies)
			{
				foreach (Type type in assemblyKVP.Value)
				{
					if (type != targetType &&
						targetType.IsAssignableFrom(type) &&
						(!mustBeInstantiable ||
						type.CheckInstantiable() ||
						(allowMonoBehaviours && typeof(MonoBehaviour).IsAssignableFrom(type))))
					{
						derivedTypes.Add(type);
					}
				}
			}

			if(includeTargetType &&
				(!mustBeInstantiable ||
				(mustBeInstantiable && targetType.CheckInstantiable())))
			{
				derivedTypes.Add(targetType);
			}

			return derivedTypes;
		}

		/// <summary>
		/// Fixes Type.Name strings so the type names of generic types are also normally returned.
		/// </summary>
		/// <returns>The sanitized type name string.</returns>
		/// <param name="type">Type.</param>
		public static string GetSanitizedTypeNameString(Type type)
		{
			if (type == null)
			{
				return "null";
			}
			else if (type.IsArray)
			{
				return GetSanitizedTypeNameString(type.GetElementType()) + "[]";
			}
			else if (type.IsGenericType)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append(type.Name.Substring(0, type.Name.Length - 2));
				for (int index = 0; index < type.GetGenericArguments().Length; index++)
				{
					Type argument = type.GetGenericArguments()[index];
					if (index < type.GetGenericArguments().Length - 1)
					{
						sb.Append(GetSanitizedTypeNameString(argument) + ", ");
					}
					else
					{
						sb.Append(GetSanitizedTypeNameString(argument));
					}
				}
				return sb.ToString();
			}

			return type.Name;
		}
	}
}