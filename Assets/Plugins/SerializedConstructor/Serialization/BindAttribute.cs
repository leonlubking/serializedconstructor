﻿// <copyright file="TypeDropdownAttribute.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Indigo.Serialization
{
	/// <summary>
	/// Used by the editor when drawing a constructor parameter in a SerializedConstructor.
	/// Add this attribute to a constructor parameter to indicate the parameter should
	/// be retrieved from the bound entries on the SerializedConstructor that were bound
	/// using the SerializedConstructor.Bind(Type, ID) method.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter, Inherited = true, AllowMultiple = false)]
	public class BindAttribute : Attribute
	{
		public readonly Type Type;
		public readonly string ID;
		public readonly bool HideInInspector;

		public BindAttribute(string id = "", bool hideInInspector = true)
		{
			ID = id;
			HideInInspector = hideInInspector;
		}

		public BindAttribute(Type type, string id, bool hideInInspector = true)
		{
			Type = type;
			ID = id;
			HideInInspector = hideInInspector;
		}
	}
}