// <copyright file="SerializedConstructorCollection.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// An array or list of <see cref="ConstructorConfiguration"/>s.
	/// </summary>
	[Serializable]
	public class ConstructorConfigurationCollection
	{
		public Type CollectionType
		{
			get
			{
				if (collectionType == null && !string.IsNullOrEmpty(collectionTypeString))
				{
					collectionType = Type.GetType(collectionTypeString);
				}
				return collectionType;
			}
			set
			{
				collectionType = value;
				collectionTypeString = value.AssemblyQualifiedName;

				IsArray = value.IsArray;

				ElementType = IsArray ? value.GetElementType() : value.GetGenericArguments()[0];
			}
		}
		private Type collectionType;
		[SerializeField] private string collectionTypeString;

		public Type ElementType
		{
			get
			{
				if(elementType == null && !string.IsNullOrEmpty(elementTypeString))
				{
					elementType = Type.GetType(elementTypeString);
				}
				return elementType;
			}
			private set
			{
				elementType = value;
				elementTypeString = value.AssemblyQualifiedName;
			}
		}
		private Type elementType;
		[SerializeField] private string elementTypeString;

		public bool IsArray { get; private set; }
		public bool IsList => !IsArray;

		public List<ConstructorConfiguration> Elements = new List<ConstructorConfiguration>();

		public ConstructorConfigurationCollection(Type collectionType, bool isArray)
		{
			CollectionType = collectionType;
			IsArray = isArray;
		}
	}
}