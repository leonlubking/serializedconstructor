// <copyright file="TypeDropdownAttribute.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Indigo.Serialization
{
	/// <summary>
	/// Used by the editor when drawing a constructor parameter in a SerializedConstructor.
	/// Add this attribute to a constructor parameter to indicate the parameter should
	/// use the given default type and constructor index. You can also indicate if further
	/// selection should be locked. Note that this requires inspecting the object to work.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter, Inherited = true, AllowMultiple = false)]
	public class DefaultAttribute : Attribute
	{
		public readonly Type SelectedType;
		public readonly int SelectedConstructorIndex;
		public readonly bool LockTypeSelection;
		public readonly bool LockConstructorSelection;

		public DefaultAttribute(Type selectedType,
			int selectedConstructorIndex = 0,
			bool lockTypeSelection = true,
			bool lockConstructorSelection = true)
		{
			SelectedType = selectedType;
			SelectedConstructorIndex = selectedConstructorIndex;
			LockTypeSelection = lockTypeSelection;
			LockConstructorSelection = lockConstructorSelection;
		}
	}
}