// <copyright file="ParameterDrawerHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Reflection;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// Helper class for drawing <see cref="IParameterDrawer"/>s.
	/// </summary>
	public class ParameterDrawerHelper
	{
		private Attribute attribute;
		private IParameterDrawer drawer;
		private ParameterInfo parameterInfo;

		public ParameterDrawerHelper(Attribute attribute, ParameterInfo parameterInfo, IParameterDrawer drawer)
		{
			this.attribute = attribute;
			this.parameterInfo = parameterInfo;
			this.drawer = drawer;
		}

		public float GetParameterHeight(float lineHeight, object currentValue, string parameterName)
		{
			return drawer.GetParameterHeight(attribute, lineHeight, currentValue, parameterName);
		}

		public object DrawParameter(Rect position, object currentValue, string parameterName, string tooltip)
		{
			return drawer.DrawParameter(parameterInfo, attribute, position, currentValue, parameterName, tooltip);
		}
	}
}