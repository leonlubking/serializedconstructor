// <copyright file="SerializedConstructorDrawer.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Indigo.Serialization
{
	[CustomPropertyDrawer(typeof(SerializedConstructorBase), true)]
	public class SerializedConstructorDrawer : PropertyDrawer
	{
		private float HEIGHT_PER_LINE => EditorGUIUtility.singleLineHeight;
		private float INDENT_DISTANCE => 12;

		private int currentArraySize;

		private FoldOutHelper foldOutInfo = new FoldOutHelper();
		private CustomDrawerHelper customDrawerHelper = new CustomDrawerHelper();

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position = new Rect(position.x, position.y, position.width, HEIGHT_PER_LINE);

			property.FindPropertyRelative("fieldPath").stringValue = property.propertyPath;
			property.FindPropertyRelative("containingObject").objectReferenceValue = property.serializedObject.targetObject;

			SerializedConstructorBase serializedInstance = SerializedPropertyHelper.GetValueForSerializedProperty<SerializedConstructorBase>(property);
			if(serializedInstance == null)
			{
				return;
			}

			Type governingType = GetGoverningType(serializedInstance);
			if(serializedInstance.Configuration == null)
			{
				// forces a configuration to be added.
				serializedInstance.OnAfterDeserialize();
			}

			// required for drawing arrays of SerializedConstructors
			string name = "";
			ReferencePath path = GetStartPath(property, out name);

			if (DrawConfiguration(ref position,
				serializedInstance.Configuration,
				name,
				governingType,
				serializedInstance,
				property,
				path,
				SerializedPropertyHelper.GetAttributesForSerializedProperty(property),
				property.FindPropertyRelative("hasPrefabModifications").boolValue))
			{
				ProcessChangeMade(property);
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float requiredHeight = HEIGHT_PER_LINE; // header
			SerializedConstructorBase serializedInstance = SerializedPropertyHelper.GetValueForSerializedProperty<SerializedConstructorBase>(property);
			if (serializedInstance == null || serializedInstance.Configuration == null)
			{
				return requiredHeight;
			}

			string name = "";
			ReferencePath path = GetStartPath(property, out name);
			requiredHeight += GetHeightForConfiguration(serializedInstance.Configuration, path, serializedInstance);
			return requiredHeight;
		}

		public override bool CanCacheInspectorGUI(SerializedProperty property)
		{
			return false;
		}

		private ReferencePath GetStartPath(SerializedProperty property, out string name)
		{
			string pathName = property.name.Substring(0, 1).ToUpper() + property.name.Substring(1, property.name.Length - 1);
			name = pathName;

			if (property.propertyPath.EndsWith("]"))
			{
				int startIndex = property.propertyPath.LastIndexOf('[') + 1;
				int endIndex = property.propertyPath.LastIndexOf(']');
				name = property.propertyPath.Substring(startIndex, endIndex - startIndex);
			}

			ReferencePath path = new ReferencePath();
			return path.CloneAndAddString(property.propertyPath);
		}

		private float GetHeightForConfiguration(ConstructorConfiguration configuration, ReferencePath path,
			SerializedConstructorBase serializedInstance, bool ignoreFoldoutState = false)
		{
			float height = 0;
			if ((!ignoreFoldoutState && !foldOutInfo.IsFoldedOut(path)) || string.IsNullOrEmpty(configuration.SelectedTypeName))
			{
				return height;
			}
			bool shouldShowConstructor = ShouldDrawConstructorSelection(configuration.SelectedType);
			bool isUnityEngineObject = configuration.SelectedType != null && typeof(UnityEngine.Object).IsAssignableFrom(configuration.SelectedType);

			if (configuration.ParameterValues == null ||
				configuration.ParameterValues.Length == 0)
			{
				return height + (shouldShowConstructor || isUnityEngineObject ? HEIGHT_PER_LINE : 0);
			}

			height += (shouldShowConstructor || isUnityEngineObject ? HEIGHT_PER_LINE : 0);

			for (int i = 0; i < configuration.ParameterValues.Length; i++)
			{
				ReferencePath parameterPath = path.CloneAndAddCollectionIndex(i);
				object value = configuration.ParameterValues[i];
				Type type = configuration.ParameterTypes[i];
				bool isList = type.IsList();
				bool isArray = type.IsArray;
				ParameterDrawerHelper propertyDrawer = customDrawerHelper.GetCustomParameterDrawerForParameterInfo(configuration.ParameterInfos[i]);

				if(!isList && !isArray && propertyDrawer != null)
				{
					height += propertyDrawer.GetParameterHeight(HEIGHT_PER_LINE, value, configuration.ParameterInfos[i].Name);
				}
				else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
				{
					if (configuration.SelectedConstructorIndex != -1) // if configured as a constructor parameter
					{
						height += GetHeightForType(type);
					}
					else
					{
						// if configured as the value to return, not using a constructor
						// nothing, since there is no constructor selection but height was added for it
					}
				}
				else if (isArray || isList)
				{
					ConstructorConfigurationCollection constructorConfigurationCollection = value as ConstructorConfigurationCollection;
					if (constructorConfigurationCollection != null)
					{
						value = constructorConfigurationCollection.Elements;
					} 

					height += value == null ? HEIGHT_PER_LINE : GetIListHeight((IList)value, type, propertyDrawer, parameterPath,
						serializedInstance);
				}
				else if (!SerializedConstructorBase.CanSerializeType(type))
				{
					height += GetHeightForNestedConfiguration((ConstructorConfiguration)value, parameterPath, serializedInstance);
				}
				else
				{
					height += GetHeightForType(type);
				}
			}

			return height;
		}

		private float GetHeightForNestedConfiguration(ConstructorConfiguration nestedConfiguration, ReferencePath parameterPath,
			SerializedConstructorBase serializedInstance)
		{
			if (nestedConfiguration != null)
			{
				return HEIGHT_PER_LINE + GetHeightForConfiguration(nestedConfiguration, parameterPath, serializedInstance);
			}
			else
			{
				return HEIGHT_PER_LINE; // type selection
			}
		}

		private Type GetGoverningType(SerializedConstructorBase serializedInstance)
		{
			Type governingType = serializedInstance.GetType();
			governingType = governingType.BaseType;
			while (!governingType.IsGenericType && governingType.GetGenericTypeDefinition() != typeof(SerializedConstructor<>))
			{
				governingType = governingType.BaseType;
				if (governingType == null)
				{
					Debug.LogError("Type was not derived from SerializedInstance<>.");
					return null;
				}
			}
			return governingType.GenericTypeArguments[0];
		}

		private bool DrawConfiguration(ref Rect position,
			ConstructorConfiguration constructorConfiguration,
			string name,
			Type governingType,
			SerializedConstructorBase serializedInstance,
			SerializedProperty serializedInstanceProperty,
			ReferencePath path,
			IEnumerable<Attribute> attributes,
			bool drawBoldLabel = false)
		{
			float indentedStartingX = EditorGUI.IndentedRect(position).x;
			string currentSelection = "None";

			if(constructorConfiguration.GoverningType != governingType)
			{
				Debug.LogError("Governing type " + constructorConfiguration.GoverningType.GetGenericName() +
					" of constructor configuration does not match " + governingType.GetGenericName() +
					" at path " + path.FullPath);
			}
			bool isNone = string.IsNullOrEmpty(constructorConfiguration.SelectedTypeName);

			Rect currentPosition = new Rect(position.x, position.y, position.width, HEIGHT_PER_LINE);

			bool drawFoldout = constructorConfiguration.ParameterValues?.Length > 0 && GetHeightForConfiguration(constructorConfiguration, path, serializedInstance, true) > 0;
			if (drawFoldout)
			{
				currentPosition = new Rect(position.x + INDENT_DISTANCE, position.y, 20f + indentedStartingX, HEIGHT_PER_LINE);
				bool wasGUIEnabled = GUI.enabled;
				GUI.enabled = wasGUIEnabled && !isNone;
				bool wasFoldedOut = foldOutInfo.IsFoldedOut(path);
				bool isFoldedOut = EditorGUI.Foldout(currentPosition, wasFoldedOut, "", true);
				foldOutInfo.SetIsFoldedOut(path, isFoldedOut);
				GUI.enabled = wasGUIEnabled;

				if(wasFoldedOut != isFoldedOut)
				{
					return true;
				}
			}

			currentSelection = isNone ? currentSelection : constructorConfiguration.SelectedTypeName;

			ParameterTooltipAttribute tooltip = GetAttribute<ParameterTooltipAttribute>(attributes);
			DefaultAttribute defaultAttribute = GetAttribute<DefaultAttribute>(attributes);

			position = new Rect(currentPosition.x, currentPosition.y, position.width - (drawFoldout ? INDENT_DISTANCE : 0), HEIGHT_PER_LINE);
			Type typeToConstruct = DrawTypeSelection(position, name, governingType, currentSelection, defaultAttribute,
				constructorConfiguration, tooltip?.Text, drawBoldLabel);

			if (constructorConfiguration.SelectedType != typeToConstruct)
			{
				constructorConfiguration.SwitchType(typeToConstruct);
				foldOutInfo.SetIsFoldedOut(path, typeToConstruct != null);
				return true;
			}
			else if (typeToConstruct == null || !foldOutInfo.IsFoldedOut(path))
			{
				position = new Rect(position.x, position.y + HEIGHT_PER_LINE, position.width, HEIGHT_PER_LINE);
				return false;
			}

			int newConstructorIndex;
			if (defaultAttribute != null &&
				defaultAttribute.SelectedType == typeToConstruct &&
				(defaultAttribute.LockConstructorSelection || !constructorConfiguration.HasBeenSetToDefaultConstructorIndex))
			{
				if (typeToConstruct.GetConstructors().Length > defaultAttribute.SelectedConstructorIndex)
				{
					newConstructorIndex = DrawConstructionMethodSelection(ref position, typeToConstruct,
						constructorConfiguration.SelectedConstructorIndex);
				}
				else
				{
					constructorConfiguration.HasBeenSetToDefaultConstructorIndex = true;
					
					bool guiWasEnabled = GUI.enabled;
					if (defaultAttribute.LockConstructorSelection)
					{
						GUI.enabled = false;
					}
					newConstructorIndex = DrawConstructionMethodSelection(ref position, typeToConstruct,
						defaultAttribute.SelectedConstructorIndex);
					GUI.enabled = guiWasEnabled;
				}
			}
			else
			{
				newConstructorIndex = DrawConstructionMethodSelection(ref position, typeToConstruct, constructorConfiguration.SelectedConstructorIndex);
			}

			if (constructorConfiguration.SelectedConstructorIndex != newConstructorIndex)
			{
				constructorConfiguration.SwitchToConstructor(newConstructorIndex);
				foldOutInfo.SetIsFoldedOut(path, true);
				return true;
			}
			position = new Rect(position.x + INDENT_DISTANCE, position.y + HEIGHT_PER_LINE, position.width - INDENT_DISTANCE, HEIGHT_PER_LINE);

			if(constructorConfiguration.ParameterValues == null)
			{
				return false;
			}
			
			for (int i = 0; i < constructorConfiguration.ParameterValues.Length; i++)
			{
				Type type = constructorConfiguration.ParameterTypes[i];
				// serialize Type as string
				if(type == typeof(Type))
				{
					type = typeof(string);
				}
				else if (type.IsArray && type.GetElementType() == typeof(Type))
				{
					type = typeof(string[]);
				}
				else if (type.IsList() && type.GetGenericArguments()[0] == typeof(Type))
				{
					type = typeof(List<string>);
				}

				ReferencePath nestedPath = path.CloneAndAddCollectionIndex(i);
				bool isList = type.IsList();
				bool isArray = type.IsArray;
				float height = GetHeightForType(type);
				position = new Rect(position.x, position.y, position.width, height);
				object currentObject = constructorConfiguration.ParameterValues[i];
				string parameterName = constructorConfiguration.ParameterInfos[i].Name;
				ParameterDrawerHelper propertyDrawer = customDrawerHelper.GetCustomParameterDrawerForParameterInfo(constructorConfiguration.ParameterInfos[i]);

				ParameterTooltipAttribute propertyTooltip = null;
				IEnumerable<Attribute> customAttributes = null;
				if (!typeof(UnityEngine.Object).IsAssignableFrom(constructorConfiguration.ParameterInfos[i].ParameterType))
				{
					customAttributes = constructorConfiguration.ParameterInfos[i]?.GetCustomAttributes();
					propertyTooltip = GetAttribute<ParameterTooltipAttribute>(customAttributes);
				}

				if (!isList && !isArray && propertyDrawer != null)
				{
					float propertyHeight = propertyDrawer.GetParameterHeight(HEIGHT_PER_LINE, currentObject, parameterName);
					
					object newValue = propertyDrawer.DrawParameter(position, currentObject, parameterName, propertyTooltip?.Text);
					if ((currentObject == null && newValue != null) ||
						(currentObject != null && !currentObject.Equals(newValue)))
					{
						constructorConfiguration.ParameterValues[i] = newValue;
						return true;
					}
					position = new Rect(position.x, position.y + propertyHeight, position.width, HEIGHT_PER_LINE);
				}
				else if (typeof(UnityEngine.Object).IsAssignableFrom(type))
				{
					object newValue = EditorGUI.ObjectField(position, new GUIContent(parameterName, propertyTooltip?.Text),
						(UnityEngine.Object)currentObject, type, false);
					if(currentObject != newValue)
					{
						constructorConfiguration.ParameterValues[i] = newValue;
						return true;
					}
					position = new Rect(position.x, position.y + height, position.width, HEIGHT_PER_LINE);
				}
				else if (isArray || isList || type == typeof(string))
				{
					if (currentObject == null)
					{
						EditorGUI.LabelField(position, new GUIContent(parameterName, propertyTooltip?.Text),
							new GUIContent("null"), GetNonBoldLabelStyle());
						if (GUI.Button(new Rect(position.x + position.width - 21f, position.y, 20f, 15f), "+"))
						{
							if (isArray || isList)
							{	
								if (!SerializedConstructorBase.CanSerializeType(type))
								{
									constructorConfiguration.ParameterValues[i] =
										new ConstructorConfigurationCollection(type, isArray);
								}
								else
								{
									constructorConfiguration.ParameterValues[i] = Activator.CreateInstance(type, 1);
								}
								foldOutInfo.SetIsFoldedOut(nestedPath, true);
								return true;
							}
							else
							{
								constructorConfiguration.ParameterValues[i] = "";
								return true;
							}
						}
						position = new Rect(position.x, position.y + height, position.width, HEIGHT_PER_LINE);
					}
					else if (GUI.Button(new Rect(position.x + position.width - 21f, position.y, 20f, 15f), "x"))
					{
						constructorConfiguration.ParameterValues[i] = null;
						currentObject = null;
						return true;
					}

					ConstructorConfigurationCollection constructorConfigurationCollection = currentObject as ConstructorConfigurationCollection;
					if (constructorConfigurationCollection != null)
					{
						currentObject = constructorConfigurationCollection.Elements;
					}

					if (currentObject != null && (isArray || isList))
					{
						// delegate is required due to the asynchronous nature of the context menu used to delete elements.
						int currentIndex = i;
						Action<IList> replaceValueDelegate = delegate (IList arrayOrList)
						{
							constructorConfiguration.ParameterValues[currentIndex] = arrayOrList;
						};
						bool changed = false;
						IList returnList = DrawIList((IList)currentObject,
							type, parameterName, position, nestedPath, serializedInstanceProperty,
							propertyDrawer, replaceValueDelegate, serializedInstance, serializedInstanceProperty,
							customAttributes, propertyTooltip?.Text, out changed);
						if (constructorConfigurationCollection == null)
						{
							constructorConfiguration.ParameterValues[i] = returnList;
						}
						else
						{
							constructorConfigurationCollection.Elements = (List<ConstructorConfiguration>)returnList;
						}
						if (changed)
						{
							return true;
						}
						position = new Rect(position.x, position.y + GetIListHeight((IList)currentObject,
							type, propertyDrawer, nestedPath, serializedInstance), position.width, position.height);
					}
					else if (currentObject != null && type == typeof(string))
					{
						string newValue = EditorGUI.TextField(
							new Rect(position.x, position.y, position.width - 22f, position.height),
							new GUIContent(parameterName, propertyTooltip?.Text), (string)currentObject);
						if((string)currentObject != newValue)
						{
							constructorConfiguration.ParameterValues[i] = newValue;
							return true;
						}
						position = new Rect(position.x, position.y + height, position.width, HEIGHT_PER_LINE);
					}
				}
				else if (!SerializedConstructorBase.CanSerializeType(type))
				{
					if (currentObject == null)
					{
						constructorConfiguration.ParameterValues[i] = new ConstructorConfiguration(type, null);
						return true;
					}

					ConstructorConfiguration nestedConstructorConfiguration = (ConstructorConfiguration)currentObject;

					float currentIndentedX = position.x;
					float currentWidth = position.width;
					bool changed = DrawConfiguration(ref position, nestedConstructorConfiguration, parameterName,
						type, serializedInstance, serializedInstanceProperty, nestedPath,
						constructorConfiguration.ParameterInfos[i].GetCustomAttributes());
					if (changed)
					{
						return true;
					}
					position = new Rect(currentIndentedX, position.y, currentWidth, position.height);
				}
				else
				{
					object newValue = DrawValueType(currentObject, type, parameterName, propertyTooltip?.Text, position);
					bool elementChanged = (newValue == null && currentObject != null) ||
						(newValue != null && !newValue.Equals(currentObject));
					if (elementChanged)
					{
						constructorConfiguration.ParameterValues[i] = newValue;
						return true;
					}
					position = new Rect(position.x, position.y + height, position.width, HEIGHT_PER_LINE);
				}
			}

			return false;
		}

		private Type DrawTypeSelection(Rect position, string name, Type governingType,
			string currentSelection, DefaultAttribute defaultAttribute,
			ConstructorConfiguration constructorConfiguration, string tooltip, bool drawBold = false)
		{
			if(defaultAttribute != null && 
				(defaultAttribute.LockTypeSelection || constructorConfiguration.HasBeenSetToDefaultType == false))
			{
				if (!governingType.IsAssignableFrom(defaultAttribute.SelectedType))
				{
					Debug.LogError(name + " set to default type " + defaultAttribute.SelectedType.Name +
						" but it is not derived from " + governingType.Name + "!");
				}
				else if(!defaultAttribute.SelectedType.CheckInstantiable())
				{
					Debug.LogError(name + " set to default type " + defaultAttribute.SelectedType.Name +
						" but it is not instantiable!");
				}
				else
				{
					constructorConfiguration.HasBeenSetToDefaultType = true;

					bool guiWasEnabled = GUI.enabled;
					if (defaultAttribute.LockTypeSelection)
					{
						GUI.enabled = false;
					}
					Type selection = TypeDropdownAttributeDrawer.DrawTypeSelection(position, new GUIContent(name, tooltip),
						governingType, true, true, defaultAttribute.SelectedType.AssemblyQualifiedName, true, true, drawBold);
					GUI.enabled = guiWasEnabled;
					return selection;
				}
			}

			return TypeDropdownAttributeDrawer.DrawTypeSelection(position, new GUIContent(name, tooltip),
				governingType, true, true, currentSelection, true, true, drawBold);
		}

		private T GetAttribute<T>(IEnumerable<Attribute> attributes) where T : Attribute
		{
			T targetAttribute = null;
			if (attributes != null)
			{
				foreach (Attribute attribute in attributes)
				{
					targetAttribute = attribute as T;
					if (targetAttribute != null)
					{
						return targetAttribute;
					}
				}
			}

			return targetAttribute;
		}

		private GUIStyle GetNonBoldLabelStyle()
		{
			GUIStyle nonBoldLabelStyle = new GUIStyle(EditorStyles.label)
			{
				fontStyle = FontStyle.Normal
			};
			return nonBoldLabelStyle;
		}

		private void ProcessChangeMade(SerializedProperty property)
		{
			Undo.RegisterCompleteObjectUndo(property.serializedObject.targetObject, "change");
			property.serializedObject.Update();
			EditorUtility.SetDirty(property.serializedObject.targetObject); // required to apply changes to prefab instances.
		}

		private float GetIListHeight(
			IList arrayOrList,
			Type type,
			ParameterDrawerHelper propertyDrawer,
			ReferencePath path,
			SerializedConstructorBase serializedInstance)
		{
			float height = HEIGHT_PER_LINE; // array header itself
			bool isFoldedOut = foldOutInfo.IsFoldedOut(path);
			if (!isFoldedOut)
			{
				return height;
			}

			height += HEIGHT_PER_LINE; // size display
			Type elementType = ArrayOrListHelper.GetArrayOrListElementType(type);
			if (propertyDrawer != null)
			{
				for (int i = 0; i < arrayOrList.Count; i++)
				{
					object value = (object)arrayOrList[i];
					height += propertyDrawer.GetParameterHeight(HEIGHT_PER_LINE, value, "Element " + i);
				}
			}
			else if (!SerializedConstructorBase.CanSerializeType(elementType))
			{
				for (int i = 0; i < arrayOrList.Count; i++)
				{
					object value = (object)arrayOrList[i];
					ReferencePath nestedPath = path.CloneAndAddCollectionIndex(i);
					height += GetHeightForNestedConfiguration((ConstructorConfiguration)value, nestedPath, serializedInstance);
				}
			}
			else
			{
				height += GetHeightForType(elementType) * arrayOrList.Count; // per entry
			}

			return height;
		}

		private IList DrawIList(
			IList arrayOrList,
			Type type,
			string parameterName,
			Rect position,
			ReferencePath path,
			SerializedProperty property,
			ParameterDrawerHelper propertyDrawer,
			Action<IList> replaceValueDelegate,
			SerializedConstructorBase serializedInstance,
			SerializedProperty serializedInstanceProperty,
			IEnumerable<Attribute> attributes,
			string tooltip,
			out bool changed)
		{
			bool wasFoldedOut = foldOutInfo.IsFoldedOut(path);
			bool isFoldedOut = EditorGUI.Foldout(new Rect(position.x + 12f, position.y, position.width - 28f, HEIGHT_PER_LINE),
				wasFoldedOut, new GUIContent(parameterName, tooltip));
			foldOutInfo.SetIsFoldedOut(path, isFoldedOut);
			changed = wasFoldedOut != isFoldedOut;
			if (!isFoldedOut)
			{
				return arrayOrList;
			}

			position = new Rect(position.x + INDENT_DISTANCE, position.y + HEIGHT_PER_LINE,
				position.width - INDENT_DISTANCE, position.height);

			GUI.SetNextControlName(parameterName);
			int enteredSize = EditorGUI.IntField(new Rect(position.x, position.y, position.width - 44f, position.height),
				"Size", GUI.GetNameOfFocusedControl() == parameterName ? currentArraySize : arrayOrList.Count);
			if (GUI.GetNameOfFocusedControl() == parameterName)
			{
				this.currentArraySize = enteredSize;
			}
			int size = arrayOrList.Count;
			if (GUI.GetNameOfFocusedControl() == parameterName &&
				Event.current.type == EventType.KeyUp &&
				Event.current.keyCode == KeyCode.Return)
			{
				size = currentArraySize;
			}

			Type elementType = ArrayOrListHelper.GetArrayOrListElementType(type);
			bool isNestedConstructor = !SerializedConstructorBase.CanSerializeType(elementType);
			Type collectionType = isNestedConstructor ? typeof(List<ConstructorConfiguration>) : type;

			if (size > arrayOrList.Count)
			{
				IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, size);
				for (int i = 0; i < size; i++)
				{
					if (arrayOrList.Count > i)
					{
						newArrayOrList[i] = arrayOrList[i];
					}
					else if (arrayOrList.Count > 0)
					{
						newArrayOrList[i] = isNestedConstructor ? new ConstructorConfiguration(elementType, null) : arrayOrList[arrayOrList.Count - 1];
					}
				}
				arrayOrList = newArrayOrList;
				changed = true;
				return arrayOrList;
			}
			else if (size < arrayOrList.Count)
			{
				IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, size);
				for (int i = 0; i < size; i++)
				{
					newArrayOrList[i] = arrayOrList[i];
				}
				arrayOrList = newArrayOrList;
				changed = true;
				return arrayOrList;
			}

			if (GUI.Button(new Rect(position.x + position.width - 42f, position.y, 20f, 15f), "+"))
			{
				IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, arrayOrList.Count + 1);
				for (int i = 0; i < arrayOrList.Count; i++)
				{
					newArrayOrList[i] = arrayOrList[i];
				}
				newArrayOrList[arrayOrList.Count] = isNestedConstructor ? new ConstructorConfiguration(elementType, null) : arrayOrList[arrayOrList.Count - 1];
				arrayOrList = newArrayOrList;
				changed = true;
				return arrayOrList;
			}

			bool guiEnabled = GUI.enabled;
			GUI.enabled = guiEnabled && arrayOrList.Count > 0;
			if (GUI.Button(new Rect(position.x + position.width - 21f, position.y, 20f, 15f), "-"))
			{
				IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, arrayOrList.Count - 1);
				for (int i = 0; i < arrayOrList.Count - 1; i++)
				{
					newArrayOrList[i] = arrayOrList[i];
				}
				arrayOrList = newArrayOrList;
				changed = true;
				return arrayOrList;
			}
			GUI.enabled = guiEnabled;

			if (arrayOrList.Count == 0 || !isFoldedOut)
			{
				changed = false;
				return arrayOrList;
			}

			for (int arrayOrListIndex = 0; arrayOrListIndex < arrayOrList.Count; arrayOrListIndex++)
			{
				object element = arrayOrList[arrayOrListIndex];

				int currentIndex = arrayOrListIndex; // cache in foreach scope for use in async delegates
				position = new Rect(position.x, position.y + HEIGHT_PER_LINE,
					position.width, position.height);

				if (isNestedConstructor)
				{
					if (element == null)
					{
						arrayOrList[arrayOrListIndex] = element = new ConstructorConfiguration(elementType, null);
					}

					ConstructorConfiguration nestedConstructorConfiguration = (ConstructorConfiguration)element;
					ReferencePath nestedPath = path.CloneAndAddCollectionIndex(arrayOrListIndex);

					float currentIndentedX = position.x;
					float currentWidth = position.width;
					bool nestedConfigurationChanged = DrawConfiguration(ref position, nestedConstructorConfiguration,
						"Element " + arrayOrListIndex.ToString(), elementType, serializedInstance,
						serializedInstanceProperty,	nestedPath, attributes);

					position = new Rect(currentIndentedX,
						position.y - HEIGHT_PER_LINE,
						currentWidth,
						position.height);

					if (nestedConfigurationChanged)
					{
						changed = true;
						return arrayOrList;
					}
				}
				else
				{
					object newValue = null;

					if (typeof(UnityEngine.Object).IsAssignableFrom(elementType))
					{
						newValue = EditorGUI.ObjectField(position, new GUIContent("Element " + arrayOrListIndex.ToString()),
							(UnityEngine.Object)element, elementType, false);
					}
					else
					{
						newValue = propertyDrawer != null ? propertyDrawer.DrawParameter(position, element,
							"Element " + currentIndex, null) : DrawValueType(element, elementType,
							"Element " + currentIndex,	null, position);
					}

					bool elementChanged = (newValue == null && element != null) || (newValue != null && !newValue.Equals(element));
					if (elementChanged)
					{
						arrayOrList[currentIndex] = newValue;
						changed = true;
						return arrayOrList;
					}
				}
				Event currentEvent = Event.current;
				if (position.Contains(currentEvent.mousePosition) && currentEvent.type == EventType.ContextClick)
				{
					GenericMenu menu = new GenericMenu();

					if (currentIndex > 0)
					{
						menu.AddItem(new GUIContent("Move up"), false,
							delegate
							{
								object otherElement = arrayOrList[currentIndex - 1];
								arrayOrList[currentIndex] = otherElement;
								arrayOrList[currentIndex - 1] = element;
								ProcessChangeMade(property);
							});
					}
					if (currentIndex != arrayOrList.Count - 1)
					{
						menu.AddItem(new GUIContent("Move down"), false,
							delegate
							{
								object otherElement = arrayOrList[currentIndex + 1];
								arrayOrList[currentIndex] = otherElement;
								arrayOrList[currentIndex + 1] = element;
								ProcessChangeMade(property);
							});
					}
					menu.AddItem(new GUIContent("Duplicate element"), false,
						delegate
						{
							IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, arrayOrList.Count + 1);
							int offset = 0;
							for (int i = 0; i < newArrayOrList.Count; i++)
							{
								if (i == currentIndex + 1)
								{
									newArrayOrList[i] = isNestedConstructor ? new ConstructorConfiguration(elementType, null) : arrayOrList[currentIndex];
									offset = -1;
								}
								else
								{
									newArrayOrList[i] = arrayOrList[i + offset];
								}
							}
							replaceValueDelegate(newArrayOrList);
							ProcessChangeMade(property);
						});
					menu.AddItem(new GUIContent("Remove element"), false,
						delegate
						{
							IList newArrayOrList = ArrayOrListHelper.CreateNewArrayOrList(collectionType, arrayOrList.Count - 1);
							int counter = 0;
							for (int i = 0; i < arrayOrList.Count; i++)
							{
								if (i == currentIndex)
								{
									continue;
								}
								newArrayOrList[counter] = arrayOrList[i];
								counter++;
							}
							replaceValueDelegate(newArrayOrList);
							ProcessChangeMade(property);
						});
					menu.ShowAsContext();

					currentEvent.Use();
				}
			}

			changed = false;
			return arrayOrList;
		}

		private float GetHeightForType(Type type)
		{
			if(type == typeof(Sprite) ||
				type == typeof(Bounds))
			{
				return HEIGHT_PER_LINE * 3f;
			}
			else if(type == typeof(Rect))
			{
				return HEIGHT_PER_LINE * 2f;
			}

			return HEIGHT_PER_LINE;
		}

		private object DrawValueType(object value, Type type, string name, string tooltip, Rect position)
		{
			GUIContent nameLabel = new GUIContent(name, tooltip);
			if (type == typeof(double))
			{
				return EditorGUI.DoubleField(position, nameLabel, (double)value);
			}
			else if (type == typeof(float))
			{
				return EditorGUI.FloatField(position, nameLabel, (float)value);
			}
			else if (type == typeof(int))
			{
				return EditorGUI.IntField(position, nameLabel, (int)value);
			}
			else if (type == typeof(long))
			{
				return EditorGUI.LongField(position, nameLabel, (long)value);
			}
			else if (type == typeof(ulong))
			{
				return AbsLong(EditorGUI.LongField(position, nameLabel, (long)value));
			}
			else if (type == typeof(uint))
			{
				return (uint)Mathf.Abs(EditorGUI.IntField(position, nameLabel, (int)value));
			}
			else if (type == typeof(string))
			{
				return EditorGUI.TextField(position, nameLabel, (string)value);
			}
			else if (type == typeof(bool))
			{
				return EditorGUI.Toggle(position, nameLabel, (bool)value);
			}
			else if (type == typeof(Vector2))
			{
				return EditorGUI.Vector2Field(position, nameLabel, (Vector2)value);
			}
			else if (type == typeof(Vector3))
			{
				return EditorGUI.Vector3Field(position, nameLabel, (Vector3)value);
			}
			else if (type == typeof(Vector4))
			{
				return EditorGUI.Vector4Field(position, nameLabel, (Vector4)value);
			}
			else if (type == typeof(Rect))
			{
				return EditorGUI.RectField(position, nameLabel, (Rect)value);
			}
			else if (type.IsEnum)
			{
				if (type.GetCustomAttribute<FlagsAttribute>() != null)
				{
					return EditorGUI.EnumFlagsField(position, nameLabel, (Enum)value);
				}
				else
				{
					return EditorGUI.EnumPopup(position, nameLabel, (Enum)value);
				}
			}
			else if (type == typeof(Bounds))
			{
				return EditorGUI.BoundsField(position, nameLabel, (Bounds)value);
			}
			else if (type == typeof(Color))
			{
				return EditorGUI.ColorField(position, nameLabel, (Color)value);
			}
			else
			{
				Debug.LogError("Encountered unsupported type: " + type.GetGenericName());
				return null;
			}
		}

		private ulong AbsLong(long value)
		{
			if(value < 0)
			{
				return (ulong)-value;
			}
			return (ulong)value;
		}

		private bool ShouldDrawConstructorSelection(Type typeToConstruct)
		{
			if (typeToConstruct == null ||
				typeof(ScriptableObject).IsAssignableFrom(typeToConstruct) ||
				typeof(MonoBehaviour).IsAssignableFrom(typeToConstruct))
			{
				return false;
			}

			ConstructorInfo[] constructors = typeToConstruct.GetConstructors();
			int validConstructors = 0;

			for (int constructorIndex = 0; constructorIndex < constructors.Length; constructorIndex++)
			{
				//ConstructorInfo constructor = constructors[constructorIndex];
				// TODO implement attribute for ignoring constructors.
				validConstructors++;
			}

			return validConstructors > 1;
		}

		private int DrawConstructionMethodSelection(ref Rect position, Type typeToConstruct, int selectedConstructorIndex)
		{
			if (typeof(ScriptableObject).IsAssignableFrom(typeToConstruct) ||
				typeof(MonoBehaviour).IsAssignableFrom(typeToConstruct))
			{
				return -1;
			}

			ConstructorInfo[] constructors = typeToConstruct.GetConstructors();
			List<GUIContent> methodDescriptions = new List<GUIContent>();
			Dictionary<int, int> constructorIndexMappings = new Dictionary<int, int>();
			
			for (int constructorIndex = 0; constructorIndex < constructors.Length; constructorIndex++)
			{
				ConstructorInfo constructor = constructors[constructorIndex];
				// TODO implement attribute for ignoring constructors.

				constructorIndexMappings.Add(methodDescriptions.Count, constructorIndex);

				string constructorDescription = constructorIndex + ": (" + GetMethodDescription(constructor.GetParameters()) + ")";
				methodDescriptions.Add(new GUIContent(constructorDescription, ""));
			}

			if(constructors.Length <= 1)
			{
				return selectedConstructorIndex;
			}

			position = new Rect(position.x + INDENT_DISTANCE, position.y + HEIGHT_PER_LINE,
				position.width - INDENT_DISTANCE, HEIGHT_PER_LINE);

			bool guiEnabled = GUI.enabled;
			GUI.enabled = constructorIndexMappings.Count > 1;
			selectedConstructorIndex = EditorGUI.Popup(position, new GUIContent("Constructor", ""), selectedConstructorIndex, methodDescriptions.ToArray());
			GUI.enabled = guiEnabled;

			return selectedConstructorIndex;
		}

		private string GetMethodDescription(ParameterInfo[] parameters)
		{
			string methodDescription = "";
			foreach (ParameterInfo parameter in parameters)
			{
				BindAttribute bindAttribute = parameter.GetCustomAttribute<BindAttribute>();
				methodDescription += parameter.ParameterType.GetGenericName() + " " +
					parameter.Name + (bindAttribute == null ? "" : (string.IsNullOrEmpty(bindAttribute.ID) ? " (bound)" :
					" (bound to '" + (bindAttribute.Type == null ? "" : bindAttribute.Type.Name + ".") + bindAttribute.ID +
					"')")) + ", ";
			}
			if (parameters.Length == 0)
			{
				methodDescription += "empty";
			}
			else
			{
				methodDescription = methodDescription.Remove(methodDescription.Length - 2);
			}
			return methodDescription;
		}
	}
}