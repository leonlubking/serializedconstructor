// <copyright file="CustomDrawerHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// Helper class for finding the custom inspector classes related to properties drawn
	/// by a <see cref="SerializedConstructorDrawer"/>.
	/// </summary>
	public class CustomDrawerHelper
	{
		private class CustomDrawerConfiguration
		{
			public readonly Type Type;
			public readonly bool UseForChildren;
			public readonly Type DrawerType;
			public IParameterDrawer Drawer
			{
				get
				{
					if(drawer == null)
					{
						drawer = (IParameterDrawer)Activator.CreateInstance(DrawerType);
					}

					return drawer;
				}
			}
			private IParameterDrawer drawer;

			public CustomDrawerConfiguration(Type type, bool useForChildren, Type drawerType)
			{
				Type = type;
				UseForChildren = useForChildren;
				DrawerType = drawerType;
			}
		}

		private Dictionary<Type, CustomDrawerConfiguration> customDrawers;

		public CustomDrawerHelper()
		{
			customDrawers = new Dictionary<Type, CustomDrawerConfiguration>
			{
				{ typeof(HideInInspector), new CustomDrawerConfiguration(typeof(HideInInspector), true, typeof(NullDrawer)) },
				{ typeof(BindAttribute), new CustomDrawerConfiguration(typeof(BindAttribute), true, typeof(BindAttributeDrawer)) }
			};

			Type targetType = typeof(IParameterDrawer);

			foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
			{
				if (!a.FullName.StartsWith("Assembly-CSharp-Editor"))
				{
					continue;
				}

				foreach (Type type in a.ExportedTypes)
				{
					if (type != targetType && targetType.IsAssignableFrom(type))
					{
						AddCustomDrawer(type);
					}
				}
			}
		}

		public ParameterDrawerHelper GetCustomParameterDrawerForParameterInfo(ParameterInfo parameterInfo)
		{
			if(parameterInfo is BasicParameterInfo)
			{
				return null;
			}

			IEnumerable<Attribute> attributes = parameterInfo.GetCustomAttributes();
			Type parameterType = parameterInfo.ParameterType;

			foreach (Attribute attribute in attributes)
			{
				Type attributeType = attribute.GetType();
				while (attributeType != typeof(Attribute) && attributeType != typeof(PropertyAttribute))
				{
					CustomDrawerConfiguration customDrawer;
					bool hasCustomDrawer = customDrawers.TryGetValue(attributeType, out customDrawer);
					if (hasCustomDrawer &&
						(customDrawer.UseForChildren || attributeType == attribute.GetType()))
					{
						return new ParameterDrawerHelper(attribute, parameterInfo, customDrawer.Drawer);
					}
					attributeType = attributeType.BaseType;
				}
			}

			return null;
		}

		private void AddCustomDrawer(Type drawerType)
		{
			IEnumerable<CustomPropertyDrawer> customPropertyDrawerAttributes = drawerType.GetCustomAttributes<CustomPropertyDrawer>();
			if (customPropertyDrawerAttributes != null)
			{
				foreach (CustomPropertyDrawer customPropertyDrawerAttribute in customPropertyDrawerAttributes)
				{
					// the values we need are internal so we need some reflection to access them.
					Type customDrawerAttributeType = customPropertyDrawerAttribute.GetType();
					FieldInfo typeInfo = customDrawerAttributeType.GetField("m_Type",
						BindingFlags.NonPublic |
						BindingFlags.Instance |
						BindingFlags.GetField);
					FieldInfo useForChildrenInfo = customDrawerAttributeType.GetField("m_UseForChildren",
						BindingFlags.NonPublic |
						BindingFlags.Instance |
						BindingFlags.GetField);
					Type customDrawerForType = (Type)typeInfo.GetValue(customPropertyDrawerAttribute);
					bool useForChildren = (bool)useForChildrenInfo.GetValue(customPropertyDrawerAttribute);
					customDrawers.Add(customDrawerForType, new CustomDrawerConfiguration(customDrawerForType, useForChildren, drawerType));
				}
			}
		}
	}
}