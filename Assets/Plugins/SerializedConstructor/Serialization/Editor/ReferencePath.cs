// <copyright file="ReferencePath.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.Text;

namespace Indigo.Serialization
{
	/// <summary>
	/// The path to a reference in a <see cref="SerializedConstructor<>"/>, used for deciding what to fold out.
	/// </summary>
	public class ReferencePath
	{
		public string[] Path { get { return path.ToArray(); } }
		private List<string> path = new List<string>();

		public string FullPath { get; private set; }

		public ReferencePath()
		{
			path = new List<string>();
			FullPath = "";
		}

		private ReferencePath(ReferencePath referencePath)
		{
			path = new List<string>(referencePath.path);
			FullPath = referencePath.FullPath;
		}

		public ReferencePath CloneAndRemoveLastAddedPath()
		{
			ReferencePath clone = new ReferencePath(this);
			clone.path.RemoveAt(clone.path.Count - 1);
			clone.FullPath = clone.BuildFullPathFromPathList(clone.path);
			return clone;
		}

		public ReferencePath CloneAndAddCollectionIndex(int index)
		{
			return AddToClone("[" + index + "]");
		}

		public ReferencePath CloneAndAddString(string stringToAdd)
		{
			return AddToClone(stringToAdd);
		}

		private ReferencePath AddToClone(string pathString, string fullPathSuffix = "")
		{
			ReferencePath clone = new ReferencePath(this);
			clone.path.Add(pathString);
			clone.FullPath += fullPathSuffix + pathString;
			return clone;
		}

		private string BuildFullPathFromPathList(List<string> path)
		{
			StringBuilder newFullPath = new StringBuilder();

			for (int i = 0; i < path.Count; i++)
			{
				string entry = path[i];

				if (entry.StartsWith("(") || entry.StartsWith("["))
				{
					newFullPath.Append(entry);
				}
				else if (newFullPath.Length > 0)
				{
					newFullPath.Append(" > " + entry);
				}
				else
				{
					newFullPath.Append(entry);
				}
			}

			return newFullPath.ToString();
		}
	}
}