// <copyright file="SerializedPropertyHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace Indigo.Serialization
{
	public static class SerializedPropertyHelper
	{
		public static IEnumerable<Attribute> GetAttributesForSerializedProperty(SerializedProperty serializedProperty)
		{
			string path = serializedProperty.propertyPath.Replace(".Array.data[", "[");
			object obj = serializedProperty.serializedObject.targetObject;
			string[] elements = path.Split('.');
			for (int i = 0; i < elements.Length; i++)
			{
				string element = elements[i];
				if (element.Contains("[") && i < elements.Length - 1)
				{
					string elementName = element.Substring(0, element.IndexOf("["));
					int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
					obj = GetValueForCollectionMember(obj, elementName, index);
				}
				else
				{
					if (i < elements.Length - 1)
					{
						obj = GetValueForMember(obj, element);
					}
					else
					{
						if (element.IndexOf('[') != -1)
						{
							element = element.Substring(0, element.IndexOf('['));
						}
						return GetAttributesForMember(obj, element);
					}
				}
			}

			return null;
		}

		public static T GetValueForSerializedProperty<T>(SerializedProperty serializedProperty)
		{
			string path = serializedProperty.propertyPath.Replace(".Array.data[", "[");
			object obj = serializedProperty.serializedObject.targetObject;
			string[] elements = path.Split('.');
			foreach (string element in elements)
			{
				if (element.Contains("["))
				{
					string elementName = element.Substring(0, element.IndexOf("["));
					int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
					obj = GetValueForCollectionMember(obj, elementName, index);
				}
				else
				{
					obj = GetValueForMember(obj, element);
				}
			}

			return (T)obj;
		}

		private static object GetValueForMember(object source, string name)
		{
			if (source == null)
			{
				return null;
			}

			Type type = source.GetType();

			while (type != null)
			{
				FieldInfo fieldInfo = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (fieldInfo != null)
				{
					return fieldInfo.GetValue(source);
				}

				PropertyInfo propertyInfo = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (propertyInfo != null)
				{
					return propertyInfo.GetValue(source, null);
				}

				type = type.BaseType;
			}
			return null;
		}

		private static IEnumerable<Attribute> GetAttributesForMember(object source, string name)
		{
			if (source == null)
			{
				return null;
			}

			Type type = source.GetType();

			while (type != null)
			{
				FieldInfo fieldInfo = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (fieldInfo != null)
				{
					return fieldInfo.GetCustomAttributes();
				}

				PropertyInfo propertyInfo = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				if (propertyInfo != null)
				{
					return propertyInfo.GetCustomAttributes();
				}

				type = type.BaseType;
			}
			return null;
		}

		private static object GetValueForCollectionMember(object source, string name, int index)
		{
			IEnumerable enumerable = GetValueForMember(source, name) as IEnumerable;
			if (enumerable == null)
			{
				return null;
			}
			IEnumerator enumerator = enumerable.GetEnumerator();
			for (int i = 0; i <= index; i++)
			{
				if (!enumerator.MoveNext())
				{
					return null;
				}
			}
			return enumerator.Current;
		}
	}
}
#endif