// <copyright file="FoldOutHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;

namespace Indigo.Serialization
{
	/// <summary>
	/// Keeps track of the fold out state of something by a given path.
	/// </summary>
	public class FoldOutHelper
	{
		private List<ReferencePath> foldedOutPaths = new List<ReferencePath>();

		public void SetIsFoldedOut(ReferencePath path, bool isFoldedOut)
		{
			for (int i = foldedOutPaths.Count - 1; i >= 0; i--)
			{
				ReferencePath foldedOutPath = foldedOutPaths[i];
				if (foldedOutPath.FullPath == path.FullPath)
				{
					if (isFoldedOut)
					{
						return;
					}
					else
					{
						foldedOutPaths.RemoveAt(i);
					}
				}
			}

			if (isFoldedOut)
			{
				foldedOutPaths.Add(path);
			}
		}

		public bool IsFoldedOut(ReferencePath path)
		{
			foreach (ReferencePath foldedOutPath in foldedOutPaths)
			{
				if (foldedOutPath.FullPath == path.FullPath)
				{
					return true;
				}
			}

			return false;
		}
	}
}