// <copyright file="SerializedConstructor.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// Lets you select a type derived from T and configure a constructor and its parameters.
	/// These are serialized. You can then use GetOrCreateInstance to retrieve a reference to it,
	/// which is created using the configured constructor and parameters.
	/// </summary>
	/// <typeparam name="T">The type that the selected type must be assignable to.</typeparam>
	[Serializable]
	public class SerializedConstructor<T> : SerializedConstructorBase, IBindingProvider
	{
		protected override Type governingType => typeof(T);

		private T instance;

		private Dictionary<Type, Dictionary<string, object>> boundValues;
		private List<IBindingProvider> bindingProviders;

		public void AddBindingProvider(IBindingProvider bindingProvider)
		{
			if(bindingProviders == null)
			{
				bindingProviders = new List<IBindingProvider>();
			}
			if (bindingProviders.Contains(bindingProvider))
			{
				bindingProviders.Add(bindingProvider);
			}
		}

		public void RemoveBindingProvider(IBindingProvider bindingProvider)
		{
			if (bindingProviders != null)
			{
				bindingProviders.Remove(bindingProvider);
			}
		}

		public void Bind<X>(string id, X value)
		{
			Bind(typeof(X), id, value);
		}

		public void Bind(Type type, string id, object value)
		{
			if(boundValues == null)
			{
				boundValues = new Dictionary<Type, Dictionary<string, object>>
				{
					{ type, new Dictionary<string, object> { { id, value } } }
				};
			}
			else if(!boundValues.ContainsKey(type))
			{
				boundValues[type] = new Dictionary<string, object> { { id, value } };
			}
			else
			{
				boundValues[type][id] = value;
			}
		}

		public void Unbind(Type type, string id)
		{
			if (boundValues == null || !boundValues.ContainsKey(type))
			{
				return;
			}
			boundValues[type].Remove(id);
		}

		public object GetBoundValue(Type type, string id, out bool success)
		{
			if (boundValues != null && boundValues.ContainsKey(type) && boundValues[type].ContainsKey(id))
			{
				success = true;
				return boundValues[type][id];
			}
			if (bindingProviders != null)
			{
				foreach (IBindingProvider bindingProvider in bindingProviders)
				{
					bool nestedSuccess = false;
					object value = bindingProvider.GetBoundValue(type, id, out nestedSuccess);
					if(nestedSuccess)
					{
						success = true;
						return value;
					}
				}
			}

			success = false;
			return null;
		}

		public T GetOrCreateInstance()
		{
			if (instance != null)
			{
				return instance;
			}

			instance = (T)CreateInstanceFromConfiguration(configuration);

			return instance;
		}

		public T CreateInstance()
		{
			return (T)CreateInstanceFromConfiguration(configuration);
		}

		private IList CreateInstanceFromConfigurationCollection(
			ConstructorConfigurationCollection configurationCollection)
		{
			if (configurationCollection.Elements == null)
			{
				return null;
			}

			IList list = ArrayOrListHelper.CreateNewArrayOrList(configurationCollection.CollectionType,
				configurationCollection.Elements.Count);

			for (int i = 0; i < configurationCollection.Elements.Count; i++)
			{
				list[i] = CreateInstanceFromConfiguration(configurationCollection.Elements[i]);
			}
			return list;
		}

		private object CreateInstanceFromConfiguration(ConstructorConfiguration configuration)
		{
			if(configuration.SelectedType == null)
			{
				Debug.Log("No type selected for " + configuration.GoverningType.Name +
					", returning default value. Path: " + SanitizePath(fieldPath), containingObject);
				return GetDefault(configuration.GoverningType);
			}

			if (typeof(MonoBehaviour).IsAssignableFrom(configuration.SelectedType) ||
				typeof(ScriptableObject).IsAssignableFrom(configuration.SelectedType))
			{
				return configuration.ParameterValues[0];
			}
			if (configuration.ParameterValues == null || configuration.ParameterValues.Length == 0)
			{
				return Activator.CreateInstance(configuration.SelectedType);
			}

			object[] parameters = new object[configuration.ParameterValues.Length];
			for (int i = 0; i < configuration.ParameterValues.Length; i++)
			{
				object parameter = configuration.ParameterValues[i];

				if (parameter == null)
				{
					parameters[i] = parameter;
					continue;
				}

				ConstructorConfiguration nestedConstructorConfiguration = parameter as ConstructorConfiguration;
				if (nestedConstructorConfiguration != null)
				{
					parameters[i] = CreateInstanceFromConfiguration(nestedConstructorConfiguration);
					continue;
				}

				ConstructorConfigurationCollection nestedConstructorConfigurationCollection = parameter as ConstructorConfigurationCollection;
				if (nestedConstructorConfigurationCollection != null)
				{
					parameters[i] = CreateInstanceFromConfigurationCollection(nestedConstructorConfigurationCollection);
					continue;
				}

				BindingConfiguration bindingConfiguration = parameter as BindingConfiguration;
				if (bindingConfiguration != null)
				{
					bool success = false;
					parameters[i] = GetBoundValue(bindingConfiguration.Type, bindingConfiguration.ID, out success);
					if (!success)
					{
						Debug.LogWarning($"Nothing was bound for type '{bindingConfiguration.Type?.Name}' id" +
							$" '{bindingConfiguration.ID}' but it was expected for parameter {i} of type " +
							$"{configuration.SelectedType.Name}, attempting with null. Path: {SanitizePath(fieldPath)}",
							containingObject);
					}
					continue;
				}

				Type parameterType = configuration.ParameterTypes[i];
				if (parameterType == typeof(Type))
				{
					string typeString = (string)parameter;
					parameters[i] = string.IsNullOrEmpty(typeString) ? null : Type.GetType(typeString);
					continue;
				}
				else if(parameterType == typeof(Type[]))
				{
					string[] stringArray = (string[])parameter;
					Type[] typeArray = new Type[stringArray.Length];
					for (int i1 = 0; i1 < stringArray.Length; i1++)
					{
						string typeString = stringArray[i1];
						typeArray[i1] = string.IsNullOrEmpty(typeString) ? null : Type.GetType(typeString);
					}
					parameters[i] = typeArray;
					continue;
				}
				else if (parameterType == typeof(List<Type>))
				{
					List<string> stringList = (List<string>)parameter;
					List<Type> typeList = new List<Type>(stringList.Count);
					foreach(string typeString in stringList)
					{
						typeList.Add(string.IsNullOrEmpty(typeString) ? null : Type.GetType(typeString));
					}
					parameters[i] = typeList;
					continue;
				}

				parameters[i] = parameter;
			}
			
			return Activator.CreateInstance(configuration.SelectedType, parameters);
		}

#if UNITY_EDITOR
		protected override void Sanitize()
		{
			if(configuration.GoverningType != typeof(T))
			{
				configuration.SanitizeGoverningType(typeof(T));
			}

			configuration.SanitizeSelectedType(true);
		}
#endif

		public object GetDefault(Type type)
		{
			if (type.IsValueType)
			{
				return Activator.CreateInstance(type);
			}
			return null;
		}
	}
}