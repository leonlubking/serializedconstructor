﻿// <copyright file="ConstructorConfiguration.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// A configuration with which the constructor of type
	/// </summary>
	public class ConstructorConfiguration : ISerializationCallbackReceiver
	{
		public Type GoverningType => governingType;
		private Type governingType
		{
			get
			{
				if (_governingType == null && !string.IsNullOrEmpty(governingTypeName))
				{
					_governingType = Type.GetType(governingTypeName);

					if (_governingType == null)
					{
						governingTypeName = null;
					}
				}

				return _governingType;
			}
			set
			{
				_governingType = value;
				if (value == null)
				{
					governingTypeName = null;
				}
				else
				{
					governingTypeName = value.AssemblyQualifiedName;
				}
			}
		}
		private Type _governingType;
		public string GoverningTypeName => governingTypeName;
		[OdinSerialize] private string governingTypeName;

		public Type SelectedType => selectedType;
		private Type selectedType
		{
			get
			{
				if (_selectedType == null && !string.IsNullOrEmpty(selectedTypeName))
				{
					_selectedType = Type.GetType(selectedTypeName);

					if (_selectedType == null)
					{
						selectedTypeName = null;
					}
				}

				return _selectedType;
			}
			set
			{
				_selectedType = value;
				if (value == null)
				{
					selectedTypeName = null;
				}
				else
				{
					selectedTypeName = value.AssemblyQualifiedName;
				}
			}
		}
		private Type _selectedType;
		public string SelectedTypeName => selectedTypeName;
		[OdinSerialize] private string selectedTypeName;

		public int SelectedConstructorIndex => selectedConstructorIndex;
		[OdinSerialize] private int selectedConstructorIndex;

		/// <summary>
		/// Configured values for constructor parameters.
		/// These will get serialized and deserialized by the SerializedConstructor.
		/// </summary>
		[OdinSerialize] public object[] ParameterValues { get; private set; }

		public Type[] ParameterTypes { get; private set; }
		[OdinSerialize] private string[] parameterTypeNames;

		public ParameterInfo[] ParameterInfos { get; private set; }

		public bool HasBeenSetToDefaultType;
		public bool HasBeenSetToDefaultConstructorIndex;

#if UNITY_EDITOR
		public ConstructorConfiguration(Type governingType, Type typeToConstruct)
		{
			this.governingType = governingType;
			SwitchType(typeToConstruct);
		}
#endif

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if (ParameterValues?.Length > 0)
			{
				for (int i = 0; i < ParameterValues.Length; i++)
				{
					sb.AppendLine(i + ": " + ParameterValues[i]);
				}
			}

			return GetType().Name +
				", selectedConstructorIndex: " + selectedConstructorIndex +
				", selectedTypeName: " + selectedTypeName +
				", parameter values: " + sb;
		}

		public virtual void OnBeforeSerialize()
		{
			if (ParameterTypes != null)
			{
				parameterTypeNames = new string[ParameterTypes.Length];
				for (int i = 0; i < ParameterTypes.Length; i++)
				{
					parameterTypeNames[i] = ParameterTypes[i].AssemblyQualifiedName;
				}
			}
			else
			{
				parameterTypeNames = null;
			}
		}

		public void OnAfterDeserialize()
		{
#if UNITY_EDITOR
			ParameterTypes = DeserializeTypes(parameterTypeNames);
#endif
		}

		public List<T> GetReferencedUnityEngineObjects<T>() where T : UnityEngine.Object
		{
			List<T> foundObjects = new List<T>();
			if(ParameterValues == null)
			{
				return foundObjects;
			}

			foreach (object parameterValue in ParameterValues)
			{
				T obj = parameterValue as T;
				if (obj != null && !foundObjects.Contains(obj))
				{
					foundObjects.Add(obj);
				}
				ConstructorConfiguration nestedConstructorConfiguration = parameterValue as ConstructorConfiguration;
				if (nestedConstructorConfiguration != null)
				{
					foreach (T foundObject in nestedConstructorConfiguration.GetReferencedUnityEngineObjects<T>())
					{
						if (!foundObjects.Contains(obj))
						{
							foundObjects.Add(obj);
						}
					}
				}
			}

			return foundObjects;
		}

#if UNITY_EDITOR
		public void SanitizeGoverningType(Type sanitizedGoverningType)
		{
			governingType = sanitizedGoverningType;

			if(!string.IsNullOrEmpty(SelectedTypeName) && !governingType.IsAssignableFrom(selectedType))
			{
				ResetSelection();
			}

			HasBeenSetToDefaultType = false;
			HasBeenSetToDefaultConstructorIndex = false;
		}

		public void SanitizeSelectedType(bool log = false)
		{
			SanitizeParameterTypesAndValues(log);

			if (ParameterValues != null)
			{
				foreach (object val in ParameterValues)
				{
					ConstructorConfiguration nestedConfig = val as ConstructorConfiguration;
					if (nestedConfig != null)
					{
						nestedConfig.SanitizeSelectedType(log);
						continue;
					}

					ConstructorConfigurationCollection nestedConfigCollection = val as ConstructorConfigurationCollection;
					if (nestedConfigCollection != null)
					{
						foreach (ConstructorConfiguration nestedConfigElement in nestedConfigCollection.Elements)
						{
							nestedConfigElement?.SanitizeSelectedType(log);
						}
					}
				}
			}
		}

		public void SwitchType(Type type)
		{
			if(type == null)
			{
				ResetSelection();
				return;
			}

			if (!GoverningType.IsAssignableFrom(type))
			{
				Debug.LogError("Attempting to switch to type " + type?.GetGenericName() +
					" which is not assignable to " + GoverningType.GetGenericName());
				return;
			}

			this.selectedType = type;
			selectedConstructorIndex = 0;
			SetParameterTypesAndNamesForConstructorIndex(selectedConstructorIndex);
			SanitizeParameterTypesAndValues(false);
		}

		public void SwitchToConstructor(int constructorIndex)
		{
			this.selectedConstructorIndex = constructorIndex;
			SetParameterTypesAndNamesForConstructorIndex(constructorIndex);
			SanitizeParameterValuesBasedOnParameterTypes(false);
		}

		private void ResetSelection()
		{
			selectedConstructorIndex = -1;
			selectedType = null;
			ParameterValues = null;
			ParameterTypes = null;
			ParameterInfos = null;
			parameterTypeNames = null;
		}

		private void SetParameterTypesAndNamesForConstructorIndex(int newConstructorIndex)
		{
			ParameterInfos = GetMethodParameters(newConstructorIndex);

			ParameterTypes = new Type[ParameterInfos.Length];
			for (int index = 0; index < ParameterInfos.Length; index++)
			{
				ParameterTypes[index] = ParameterInfos[index].ParameterType;
			}
		}

		private ParameterInfo[] GetMethodParameters(int constructorIndex)
		{
			ParameterInfo[] methodParameters = null;
			ConstructorInfo[] constructors = SelectedType.GetConstructors();
			if(constructors == null || constructors.Length == 0)
			{
				return new ParameterInfo[0];
			}
			ConstructorInfo constructor = constructors[constructorIndex];
			methodParameters = constructor.GetParameters();
			return methodParameters;
		}

		private void SanitizeParameterTypesAndValues(bool log = true)
		{
			if (SelectedType == null)
			{
				selectedConstructorIndex = 0;
				ParameterValues = null;
				ParameterTypes = new Type[0];
				ParameterInfos = new ParameterInfo[0];
				return;
			}
			else if (typeof(ScriptableObject).IsAssignableFrom(SelectedType))
			{
				selectedConstructorIndex = -1;
				if (ParameterValues == null || ParameterValues.Length != 1 ||
					!(ParameterValues[0] != null && SelectedType.IsAssignableFrom(ParameterValues[0].GetType())))
				{
					ParameterValues = new object[1] { null };
				}

				ParameterTypes = new Type[1] { SelectedType };
				ParameterInfos = new ParameterInfo[1] { new BasicParameterInfo("ScriptableObject", typeof(ScriptableObject)) };
				return;
			}
			else if (typeof(MonoBehaviour).IsAssignableFrom(SelectedType))
			{
				selectedConstructorIndex = -1;
				if (ParameterValues == null || ParameterValues.Length != 1 ||
					!(ParameterValues[0] != null && SelectedType.IsAssignableFrom(ParameterValues[0].GetType())))
				{
					ParameterValues = new object[1] { null };
				}
				ParameterTypes = new Type[1] { SelectedType };
				ParameterInfos = new ParameterInfo[1] { new BasicParameterInfo("MonoBehaviour", typeof(MonoBehaviour)) };
				return;
			}

			int bestIndex = FindIndexOfConstructorWithBestMatchingParameters(ParameterTypes, selectedConstructorIndex);

			if (selectedConstructorIndex != bestIndex)
			{
				int previousConstructionMethodIndex = selectedConstructorIndex;
				selectedConstructorIndex = bestIndex;

				if (log)
				{
					Debug.LogWarning("Previously configured constructor index " + previousConstructionMethodIndex +
						" for type " + SelectedType.GetGenericName() + " is no longer the best match with the configured values, changing" +
						" to the best fitting constructor with index " + bestIndex + " and attempting to remap values.");
				}
			}
			
			// always do this in case some of the types no longer resolve because they've been renamed
			SetParameterTypesAndNamesForConstructorIndex(selectedConstructorIndex);
			SanitizeParameterValuesBasedOnParameterTypes(log);
		}

		private void SanitizeParameterValuesBasedOnParameterTypes(bool log = true)
		{
			List<object> oldValues = ParameterValues != null ? new List<object>(ParameterValues) : new List<object>();
			List<object> newParameterValues = new List<object>(ParameterTypes.Length);

			for (int index = 0; index < ParameterTypes.Length; index++)
			{
				Type parameterType = null;
				object defaultValue = null;
				GetDefaultValueAndTypeForParameterIndex(index, out defaultValue, out parameterType);

				if (parameterType == null)
				{
					continue;
				}

				bool set = false;
				foreach (object oldValue in oldValues)
				{
					if (oldValue == null)
					{
						if (defaultValue == null)
						{
							// In case the bound ID has changed we always use the newly generated InjectionConfiguration
							newParameterValues.Add(parameterType == typeof(BindingConfiguration) ? defaultValue : oldValue);
							oldValues.Remove(oldValue);
							set = true;
							break;
						}
						continue;
					}

					Type oldValueType = oldValue.GetType();
					if (oldValueType == parameterType)
					{
						// In case the bound ID has changed we always use the newly generated InjectionConfiguration
						newParameterValues.Add(parameterType == typeof(BindingConfiguration) ? defaultValue : oldValue);
						oldValues.Remove(oldValue);
						set = true;
						break;
					}
				}
				if (!set)
				{
					if (log)
					{
						string remainingOldValues = oldValues.Count > 0 ? "" : "none";
						foreach (object oldValue in oldValues)
						{
							remainingOldValues += oldValue != null ? oldValue.GetType().Name : "null";
							remainingOldValues += ", " + Environment.NewLine;
						}

						Debug.LogWarning("Constructor parameter " + index + " of type " + parameterType.GetGenericName() +
							" for " + selectedTypeName.Substring(0, selectedTypeName.IndexOf(',')) + " did not match any of" +
							" the assigned values, reverting to default of " + (defaultValue ?? "null") + Environment.NewLine +
							"Unmapped old values: " + remainingOldValues);
					}
					newParameterValues.Add(defaultValue);
				}
			}

			if (newParameterValues.Count == 0)
			{
				ParameterValues = null;
			}
			else
			{
				ParameterValues = newParameterValues.ToArray();
			}

			if (ParameterValues != null)
			{
				ParameterInfo[] methodParameters = SelectedType.GetConstructors()[selectedConstructorIndex].GetParameters();

				for (int i = 0; i < ParameterValues.Length; i++)
				{
					object newValue = ParameterValues[i];

					ConstructorConfiguration constructorConfiguration = newValue as ConstructorConfiguration;
					if(constructorConfiguration != null)
					{
						Type governingType = methodParameters[i].ParameterType;
						if (constructorConfiguration.GoverningType != governingType)
						{
							constructorConfiguration.SanitizeGoverningType(governingType);
						}
					}

					ConstructorConfigurationCollection constructorConfigurationCollection = newValue as ConstructorConfigurationCollection;
					if (constructorConfigurationCollection != null)
					{
						Type collectionType = methodParameters[i].ParameterType;
						if (constructorConfigurationCollection.CollectionType != collectionType)
						{
							constructorConfigurationCollection.CollectionType = collectionType;
							foreach (ConstructorConfiguration constructorConfigurationElement in constructorConfigurationCollection.Elements)
							{
								if (constructorConfigurationElement.GoverningType != constructorConfigurationCollection.ElementType)
								{
									constructorConfigurationElement.SanitizeGoverningType(constructorConfigurationCollection.ElementType);
								}
							}
						}
					}
				}
			}
		}

		private int FindIndexOfConstructorWithBestMatchingParameters(Type[] currentMethodParameterTypes, int lastSelectedIndex)
		{
			int lastSelectedIndexBiasScore = 5;
			int matchingParameterCountScore = 5;
			int matchingParameterAtCorrectIndexScore = 10;
			int matchingParameterAtDifferentIndexScore = 5;

			List<MethodBase> eligibleMethods = new List<MethodBase>();
			ConstructorInfo[] constructors = SelectedType.GetConstructors();
			foreach (ConstructorInfo constructorInfo in constructors)
			{
				eligibleMethods.Add(constructorInfo);
			}

			int[] matchScores = new int[eligibleMethods.Count];
			if (matchScores.Length > lastSelectedIndex)
			{
				matchScores[lastSelectedIndex] += lastSelectedIndexBiasScore;
			}

			int checkedMethods = 0;
			for (int methodIndex = 0; methodIndex < eligibleMethods.Count; methodIndex++)
			{
				MethodBase method = eligibleMethods[methodIndex];
				checkedMethods++;

				ParameterInfo[] parameters = method.GetParameters();
				int parametersLength = parameters.Length;

				List<int> methodParametersMatchedIndexes = new List<int>();
				if (parametersLength == currentMethodParameterTypes.Length)
				{
					matchScores[methodIndex] += matchingParameterCountScore;
				}
				for (int index = 0; index < parameters.Length; index++)
				{
					ParameterInfo parameter = parameters[index];
					int methodParametersIndex = index;
					if (currentMethodParameterTypes.Length > methodParametersIndex &&
						parameter.ParameterType == currentMethodParameterTypes[methodParametersIndex])
					{
						matchScores[methodIndex] += matchingParameterAtCorrectIndexScore;
						methodParametersMatchedIndexes.Add(index);
						continue;
					}
				}
				for (int index = 0; index < parameters.Length; index++)
				{
					ParameterInfo parameter = parameters[index];

					for (int i = 0; i < currentMethodParameterTypes.Length; i++)
					{
						if (methodParametersMatchedIndexes.Contains(i))
						{
							continue;
						}
						if (parameter.ParameterType == currentMethodParameterTypes[i])
						{
							matchScores[methodIndex] += matchingParameterAtDifferentIndexScore;
							methodParametersMatchedIndexes.Add(i);
							continue;
						}
					}
				}
			}

			if (checkedMethods == 0)
			{
				if (lastSelectedIndex != 0)
				{
					Debug.LogError("No valid constructor for use with a serialized instance found. Reverting to first constructor.");
				}

				return 0;
			}

			int methodWithBestScore = -1;
			int bestMethodScore = -1;
			for (int index = 0; index < matchScores.Length; index++)
			{
				int score = matchScores[index];
				if (bestMethodScore < score)
				{
					methodWithBestScore = index;
					bestMethodScore = score;
				}
			}

			return methodWithBestScore;
		}

		private void GetDefaultValueAndTypeForParameterIndex(int index, out object value, out Type type)
		{
			ParameterInfo[] methodParameters = SelectedType.GetConstructors()[selectedConstructorIndex].GetParameters();

			if (methodParameters.Length <= index)
			{
				value = null;
				type = null;
				return;
			}

			ParameterInfo parameterInfo = methodParameters[index];
			Type parameterType = parameterInfo.ParameterType;

			BindAttribute bindAttribute = parameterInfo.GetCustomAttribute<BindAttribute>();
			bool willBeInjected = bindAttribute != null;
			if (willBeInjected)
			{
				type = typeof(BindingConfiguration);
			}
			else if (!SerializedConstructorBase.CanSerializeType(parameterType))
			{
				if (parameterType.IsArray || parameterType.IsList())
				{
					type = typeof(ConstructorConfigurationCollection);
				}
				else
				{
					type = typeof(ConstructorConfiguration);
				}
			}
			else
			{
				type = parameterType;
			}

			if (willBeInjected)
			{
				value = new BindingConfiguration(bindAttribute.Type ?? parameterType, bindAttribute.ID);
				return;
			}
			else if (parameterInfo.HasDefaultValue && parameterInfo.DefaultValue != null)
			{
				value = parameterInfo.DefaultValue;
				return;
			}
			else if (parameterType == typeof(string))
			{
				value = null;
				return;
			}
			else if (parameterType == typeof(Type))
			{
				value = null;
				type = typeof(string);
				return;
			}
			else if (parameterType.IsArray || parameterType.IsList())
			{
				value = null;
				return;
			}
			else if (typeof(UnityEngine.Object).IsAssignableFrom(parameterType))
			{
				value = null;
				return;
			}
			else if (SerializedConstructorBase.CanSerializeType(parameterType))
			{
				value = Activator.CreateInstance(parameterType);
				return;
			}
			else
			{
				value = null;
				return;
			}
		}
#endif

		private Type[] DeserializeTypes(string[] parameterTypeNames)
		{
			if(parameterTypeNames == null)
			{
				return null;
			}

			Type[] parameterTypes = new Type[parameterTypeNames.Length];
			for (int i = 0; i < parameterTypeNames.Length; i++)
			{
				// can be null if the type no longer resolves
				parameterTypes[i] = Type.GetType(parameterTypeNames[i]);
			}
			return parameterTypes;
		}
	}
}