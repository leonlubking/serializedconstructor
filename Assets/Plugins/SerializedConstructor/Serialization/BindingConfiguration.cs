﻿// <copyright file="ConstructorConfiguration.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Indigo.Serialization
{
	/// <summary>
	/// A configuration which states which value bound by ID should be injected.
	/// </summary>
	[Serializable]
	public class BindingConfiguration
	{
		public Type Type;
		public string ID;

		public BindingConfiguration(Type type, string id)
		{
			Type = type;
			ID = id;
		}
	}
}