// <copyright file="SerializedConstructorBase.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using OdinSerializer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// Use <see cref="SerializedConstructor"/><T> instead of this class.
	/// Lets you select a type derived from T and configure a constructor and its parameters.
	/// These are serialized. You can then use GetOrCreateInstance to retrieve a reference to it,
	/// which is created using the configured constructor and parameters.
	/// </summary>
	[Serializable]
	public abstract class SerializedConstructorBase : ISerializationCallbackReceiver
	{
		public ConstructorConfiguration Configuration => configuration;
		protected ConstructorConfiguration configuration;

		protected abstract Type governingType { get; }

		[SerializeField] private byte[] serializedData;
		[SerializeField] private string serializedPrefabData;
		[SerializeField] private List<UnityEngine.Object> unityObjects;

		// used for prefab instances to break the link on the prefab's unityObjects when
		// the serializedPrefabData is different on the instance.
		[SerializeField] protected UnityEngine.Object containingObject;
		[SerializeField] protected string fieldPath;
		#pragma warning disable 0414
		[SerializeField] private bool hasPrefabModifications = false;
		#pragma warning restore 0414

		public virtual void OnBeforeSerialize()
		{
			if (configuration == null)
			{
				serializedData = null;
				serializedPrefabData = null;
				unityObjects = null;
				return;
			}

			MonoBehaviour containingMonoBehaviour = containingObject as MonoBehaviour;
			if (containingMonoBehaviour == null)
			{
				serializedData = SerializationUtility.SerializeValue(configuration, DataFormat.Binary, out unityObjects);
			}
			else
			{
				// use JSON serialization since that doesn't have prefab modification issues.
				serializedPrefabData = Encoding.UTF8.GetString(SerializationUtility.SerializeValue(configuration, DataFormat.JSON, out unityObjects));
			}

#if UNITY_EDITOR
			// When the serializedPrefabData changes from the prefab we need to break the prefab link with the unityObjects array as well.
			if (fieldPath != null &&
				containingMonoBehaviour != null &&
				!AssetDatabase.Contains(containingMonoBehaviour.gameObject) &&
				PrefabUtility.GetPrefabAssetType(containingMonoBehaviour.gameObject) != PrefabAssetType.NotAPrefab)
			{
				UnityEngine.Object target = PrefabUtility.GetCorrespondingObjectFromSource(containingMonoBehaviour);
				string modificationsPath = fieldPath + ".serializedPrefabData";

				List<PropertyModification> propertyModifications = new List<PropertyModification>(PrefabUtility.GetPropertyModifications(containingMonoBehaviour.gameObject));
				int initialCount = propertyModifications.Count;

				hasPrefabModifications = false;
				
				string unityObjectsPath = fieldPath + ".unityObjects.Array.";
				//LogModifications(initialCount, propertyModifications);
				int unityObjectOverrides = 0;
				for (int i = propertyModifications.Count - 1; i >= 0; i--)
				{
					PropertyModification propertyModification = propertyModifications[i];
					if (propertyModification.propertyPath == modificationsPath)
					{
						hasPrefabModifications = true;
					}
					if (propertyModification.propertyPath.Contains(unityObjectsPath))
					{
						unityObjectOverrides++;
					}
				}

				bool allUnityObjectsOverriden = unityObjectOverrides >= unityObjects.Count + 1; // +1 for the size parameter
				bool prefabModificationsAdded = hasPrefabModifications;
				bool shouldAddUnityObjectsOverride = unityObjectOverrides > 0 && !allUnityObjectsOverriden;

				// Don't break the prefab link if nothing's changed.
				// Do break it if either the prefab data changed or a unityEngine object changed.
				// Return to make sure we don't keep recursively serializing by setting the DelayedCall again below.
				if ((!hasPrefabModifications || prefabModificationsAdded) && !shouldAddUnityObjectsOverride)
				{
					return;
				}

				hasPrefabModifications = true;

				if (shouldAddUnityObjectsOverride)
				{
					// remove old entries so we can replace them
					for (int i = propertyModifications.Count - 1; i >= 0; i--)
					{
						PropertyModification propertyModification = propertyModifications[i];
						if (propertyModification.propertyPath.Contains(unityObjectsPath))
						{
							propertyModifications.RemoveAt(i);
							continue;
						}
					}

					propertyModifications.Insert(0, new PropertyModification()
					{
						target = target,
						propertyPath = unityObjectsPath + "size",
						value = unityObjects.Count.ToString("D", CultureInfo.InvariantCulture)
					});

					for (int i = 0; i < unityObjects.Count; i++)
					{
						propertyModifications.Add(new PropertyModification()
						{
							target = target,
							propertyPath = unityObjectsPath + "data[" + i.ToString("D", CultureInfo.InvariantCulture) + "]",
							objectReference = unityObjects[i]
						});
					}
				}

				if (!prefabModificationsAdded)
				{
					propertyModifications.Add(new PropertyModification()
					{
						target = target,
						propertyPath = modificationsPath,
						value = serializedPrefabData
					});
				}
				//LogModifications(initialCount, propertyModifications);

				// Needs to occur in a delayed call or it won't apply properly.
				EditorApplication.delayCall += () =>
				{
					PrefabUtility.SetPropertyModifications(containingMonoBehaviour.gameObject, propertyModifications.ToArray());
				};
			}
			else
			{
				hasPrefabModifications = false;
			}
#endif
		}

		public void OnAfterDeserialize()
		{
			// note that this only occurs at run-time, not in the custom inspector.
			if (!string.IsNullOrEmpty(serializedPrefabData))
			{
				//Debug.Log("Deserializing from unityObjects: " + unityObjects?.Count + ", serializedData: " + Environment.NewLine + serializedPrefabData);
				configuration = SerializationUtility.DeserializeValue<ConstructorConfiguration>(Encoding.UTF8.GetBytes(serializedPrefabData), DataFormat.JSON, unityObjects);
			}
			else if (serializedData != null && serializedData.Length > 0)
			{
				configuration = SerializationUtility.DeserializeValue<ConstructorConfiguration>(serializedData, DataFormat.Binary, unityObjects);
			}
#if UNITY_EDITOR
			else
			{
				configuration = new ConstructorConfiguration(governingType, null);
			}

			Sanitize();
#endif

			serializedPrefabData = null;
			serializedData = null;
		}

#if UNITY_EDITOR
		public static bool CanSerializeType(Type type)
		{
			if(type == null)
			{
				return true;
			}
			return type == typeof(Vector2) ||
				type == typeof(Vector3) ||
				type == typeof(Vector4) ||
				type == typeof(Rect) ||
				type == typeof(Bounds) ||
				type == typeof(Color) ||
				type == typeof(Type) ||
				typeof(UnityEngine.Object).IsAssignableFrom(type) ||
				type == typeof(string) || 
				type.IsEnum ||
				type.IsPrimitive ||
				(type.IsArray && CanSerializeType(type.GetElementType())) ||
				(type.IsList() && CanSerializeType(type.GetGenericArguments()[0]));
		}

		protected abstract void Sanitize();

		private void LogModifications(int initialCount, List<PropertyModification> propertyModifications)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < propertyModifications.Count; i++)
			{
				PropertyModification propertyModification = propertyModifications[i];
				sb.AppendLine(i + ": target: " + propertyModification.target +
					", path: " + propertyModification.propertyPath +
					", value: " + propertyModification.value +
					", objectReference: " + propertyModification.objectReference);
			}

			Debug.Log("Modifications changed from " + initialCount + " to " +
				propertyModifications.Count + Environment.NewLine + sb.ToString() +
				Environment.NewLine + "Path: " + SanitizePath(fieldPath), containingObject);
		}
#endif

		protected string SanitizePath(string fieldPath)
		{
			return fieldPath.Replace(".Array.data", "");
		}
	}
}