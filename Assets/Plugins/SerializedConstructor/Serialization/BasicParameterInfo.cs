// <copyright file="BasicParameterInfo.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Indigo.Serialization
{
	/// <summary>
	/// Basic parameter info for when you need to make your own that lets you pass a name and type.
	/// </summary>
	public class BasicParameterInfo : ParameterInfo
	{
		public override string Name => name;
		private string name;

		public override Type ParameterType => parameterType;
		private Type parameterType;

		public override ParameterAttributes Attributes => ParameterAttributes.None;
		public override IEnumerable<CustomAttributeData> CustomAttributes => null;

		public BasicParameterInfo(string name, Type parameterType) : base()
		{
			this.name = name;
			this.parameterType = parameterType;
		}

		public override object[] GetCustomAttributes(bool inherit)
		{
			return new object[0];
		}

		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return new object[0];
		}
	}
}