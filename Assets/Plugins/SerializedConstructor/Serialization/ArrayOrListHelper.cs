// <copyright file="ArrayOrListHelper.cs" company="Indigo">
// Copyright (c) 2018 Leon Lubking
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
//-----------------------------------------------------------------------

using Indigo.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Indigo.Serialization
{
	/// <summary>
	/// Helper methods to be able to treat an array or list in the same way.
	/// </summary>
	public static class ArrayOrListHelper
	{
		public static IList CreateNewArrayOrList(Type type, int length)
		{
			if (type.IsArray)
			{
				return (IList)Activator.CreateInstance(type, length);
			}
			else if (type.IsList())
			{
				IList list = (IList)Activator.CreateInstance(type, length);
				Type elementType = type.GetGenericArguments()[0];
				for (int i = 0; i < length; i++)
				{
					list.Add(GetDefaultValueForType(elementType));
				}
				return list;
			}
			return null;
		}

		public static Type GetArrayOrListElementType(Type arrayOrListType)
		{
			if (arrayOrListType.IsArray)
			{
				return arrayOrListType.GetElementType();
			}
			else if (arrayOrListType.IsList())
			{
				return arrayOrListType.GetGenericArguments()[0];
			}

			return null;
		}

		private static object GetDefaultValueForType(Type type)
		{
			if (type == typeof(string))
			{
				return null;
			}
			else if (type.IsArray || type.IsList())
			{
				return null;
			}
			else if (type.IsEnum ||
				type == typeof(double) ||
				type == typeof(float) ||
				type == typeof(int) ||
				type == typeof(long) ||
				type == typeof(string) ||
				type == typeof(bool) ||
				type == typeof(Vector2) ||
				type == typeof(Vector3) ||
				type == typeof(Vector4) ||
				type == typeof(Rect) ||
				type == typeof(Bounds) ||
				type == typeof(Color))
			{
				return Activator.CreateInstance(type);
			}

			return null;
		}
	}
}